/**
 * Copyright ©2019 Mavik Technologies LLC., Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * LockCodes component
 * -------------
 */

/**
 * Import used react components
 */
import React from "react";
import PropTypes from "prop-types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash, faAngleDown, faPlus} from "@fortawesome/free-solid-svg-icons";
import Draggable from "./Draggable";
import PageHandlerStore from "../stores/PageHandler";
import LoaderService from "./services/Loader";
import QRDataStore from "../stores/QRdata";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import ValidationService from "./services/Validation";
import Textarea from "react-validation/build/textarea";
import Button from "react-validation/build/button";

/**
 * Import used vendor components
 */

export default class LockCodes extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isDarkMode: PageHandlerStore.getValue("isDarkMode"),
            lockCodes: this.props.lockCodes,
            showContent: this.props.showContent,
            addNewCode: false,
            lockcode: "",
            lock_description: "",
        };

        this.deleteLockCodeSuccess = this.deleteLockCodeSuccess.bind(this);
        this.getLockCodesSuccess = this.getLockCodesSuccess.bind(this);
        this.addLockCode = this.addLockCode.bind(this);
        this.addLockCodeSuccess = this.addLockCodeSuccess.bind(this);
        this.addLockCodeError = this.addLockCodeError.bind(this);
        this.addCode = this.addCode.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        let self = this;
        if (e.target.name === "lockcode") {
            self.setState({lockcode: e.target.value});
        }
        if (e.target.name === "lock_description") {
            self.setState({lock_description: e.target.value});
        }
    }

    componentDidMount(){
        PageHandlerStore.on("change", () => {
            this.setState({
                isDarkMode: PageHandlerStore.getValue("isDarkMode"),
            });
        });

        if (this.props.lockCodes != undefined && this.props.lockCodes.length > 0) {
            this.setState({lockCodes: this.props.lockCodes});
        }
        else if (this.props.qrCodeId !== "") {
            this.getLockCodes();
        }
    }

    // componentDidUpdate(prevProps) {
    //     console.log(prevProps);
    // }

    getLockCodesSuccess(result) {
        this.setState({lockCodes: result.data});
    }

    getLockCodesError() {
        //TODO: error handling if needed
    }

    getLockCodes() {
        let data = {
            "searchAdditional": {},
            "qrcode_id": this.props.qrCodeId
        };

        if (this.props.role === "courier_employee") {
            LoaderService.postData("courier/lockcode/search", data, this.getLockCodesSuccess, this.getLockCodesError);
        }
        else {
            LoaderService.postData("customer/lockcode/search", data, this.getLockCodesSuccess, this.getLockCodesError);
        }
    }

    getBgColor() {
        return window.qrLocation.colors[this.state.isDarkMode]["--color-background"];
    }

    changeState(){
        this.setState({showContent: !this.state.showContent});
    }

    addLockCodeSuccess() {
        this.setState({lockcode: "", lock_description: "", addNewCode: false});
        this.getLockCodes();
    }

    addLockCodeError() {
        //TODO: error handling if needed
    }

    addLockCode(e){
        e.preventDefault();
        let data = {qrcode_id: QRDataStore.getData("id"), pincode: this.state.lockcode, description: this.state.lock_description};
        LoaderService.postData("customer/lockcode", data, this.addLockCodeSuccess, this.addLockCodeError);
    }

    addCode(){
        this.setState({addNewCode: !this.state.addNewCode, lockcode: "", lock_description: ""});
    }

    deleteLockCodeSuccess() {
        this.getLockCodes();
    }

    deleteLockCodeError() {
        //TODO: error handling if needed
    }

    deleteLockCode(item) {
        LoaderService.deleteData("customer/lockcode", {id: item.id}, this.deleteLockCodeSuccess, this.deleteLockCodeError);
    }

    /**
     * Render the component
     * @returns {*}
     */
    render() {
        let self = this;

        return (<div>
            <div className={"opener-title " + this.state.showContent}>
                <span className="subtitle highlighted"> Lock codes:</span>
                <FontAwesomeIcon icon={faAngleDown} onClick={() => {this.changeState();}} />
            </div>
            <div className={"opener-holder " + this.state.showContent}>

                { this.props.role === "customer" ?
                    <div className="row">
                        <div className="col-xs-12 text-center">
                            <button className="" onClick={() => {this.addCode();}}>
                                <FontAwesomeIcon icon={faPlus} />
                                <span className="btn-title">
                                    Add New Code
                                </span>
                            </button>
                        </div>
                    </div>
                    : null }
                { this.props.role === "customer" ? <div className={"add-code-holder " + this.state.addNewCode}>
                    <div className="row">
                        <div className="col-xs-12"></div>
                        <div className="col-xs-12 text-center subtitle highlighted">Add new lock code:</div>
                    </div>
                    <Form>
                        <div className="row">
                            <div className="col-xs-12">
                                <Input className="full-width" placeholder="lockcode*" name="lockcode" value={this.state.lockcode} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                            </div>
                            <div className="col-xs-12">
                                <Textarea className="full-width" placeholder="description*" name="lock_description" value={this.state.lock_description} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12">
                                <Button className="full-width" onClick={this.addLockCode}>
                                    <span>Add lock code</span>
                                </Button>
                            </div>
                        </div>
                    </Form>
                </div> : null }
                <div className="row decorator">
                    <div className="col-xs-12">
                        <div className="decorator-line" />
                    </div>
                </div>
                { this.state.lockCodes != undefined && this.state.lockCodes.length > 0 ?
                    this.state.lockCodes.map((item, i) => {
                        return (
                            <React.Fragment  key={i}>
                                <div style={{position: "relative", height: "80px"}}>
                                    <button className="full-width red row-button" onClick={() => {this.deleteLockCode(item);}}>
                                        <span><FontAwesomeIcon icon={faTrash} /> Delete lock code</span>
                                    </button>
                                    <Draggable
                                        display={"block"}
                                        direction={"left"}
                                        left={"0"}
                                        right={"0"}
                                        minPos={30}
                                        useClass={"SwipeAction"}
                                        iscroll={false}
                                        splashColor={self.getBgColor()}
                                        closeOnOutside={true}
                                    >
                                        <div className="row">
                                            <div className="col-xs-4 small-title v-middle text-right">
                                                Code:
                                            </div>
                                            <div className="col-xs-8 highlighted">
                                                {item.pincode}
                                            </div>
                                            <div className="col-xs-4 small-title text-right v-middle">
                                                Description:
                                            </div>
                                            <div className="col-xs-8">
                                                {item.description}
                                            </div>
                                        </div>
                                    </Draggable>
                                </div>
                                <div className="row decorator">
                                    <div className="col-xs-12">
                                        <div className="decorator-line" />
                                    </div>
                                </div>
                            </React.Fragment>
                        );
                    })
                    : <div className="row">
                        <div className="col-xs-12 text-center not-found">
                            No lock codes found
                        </div>
                    </div> }
            </div>
        </div>);
    }
}

LockCodes.propTypes = {
    lockCodes: PropTypes.array,
    qrCodeId: PropTypes.num,
    role: PropTypes.string,
    showContent: PropTypes.bool
};

LockCodes.defaultProps = {
    lockCodes: [],
    role: "",
    showContent: true
};