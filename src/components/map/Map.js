/**
 * Copyright ©2019 Mavik Technologies LLC., Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Map component
 * -------------
 * - show map
 * - add lockboxes to the map
 * - add landing zones to the map
 * - get coordinates of the markers
 *
 * Example usage:
 * this.lockboxes = [
 * {lat:28.02353349130747, lng:-82.73209454426109},
 * {lat:28.023534, lng:-82.732015}
 * ];
 * this.landingzones = [
 * {lat:28.02353349130647, lng:-82.73209454426209},
 * {lat:28.023434, lng:-82.732115}
 * ];
 * <Map center={this.center} lockboxes={this.lockboxes} langingzones={this.landingzones}/>
 */

/**
 * Import used react components
 */
import React from "react";
import PropTypes from "prop-types";

/**
 * Import used vendor components
 */
import loadGoogleMapsApi from "load-google-maps-api";

export default class Map extends React.Component {
    constructor(props) {
        super(props);

        /**
         * Set the map options
         * @type {{mapTypeControl: boolean, mapTypeId: string, scaleControl: boolean, center: *, streetViewControl: boolean, fullscreenControl: boolean, gestureHandling: string, styles: {featureType: string, stylers: {visibility: string}[], elementType: string}[], zoom: number, tilt: number}}
         */

        this.mapOptions = {
            streetViewControl: false,
            scaleControl: false,
            fullscreenControl: false,
            zoomControl: true,
            rotateControl: true,
            styles: this.props.mapStyle,
            gestureHandling: "greedy",
            mapTypeControl: false,
            mapTypeId: "hybrid",
            tilt: 0,
            zoom: 19,
            center: this.props.center
        };

        this.state = {
            editLocations: this.props.editLocations,
            editDropzone: this.props.editDropzone
        };

        this.addMap = this.addMap.bind(this);
        this.chekcProps = this.chekcProps.bind(this);
    }

    componentDidMount(){
        let self = this;

        if (!window.hasOwnProperty("google")) {
            /**
             * Load Google Maps API
             */
            loadGoogleMapsApi({key:"AIzaSyCuv4pnWSAuoNZLUQZnd6REItQVmLJmEpg&libraries=places"}).then(function (googleMaps) {
                window.google.maps = googleMaps;
                self.mapOptions.zoomControlOptions = {position: window.google.maps.ControlPosition.RIGHT_TOP};
                self.mapOptions.rotateControlOptions = {position: window.google.maps.ControlPosition.LEFT_TOP};
                self.addMap();
                self.chekcProps();
            });
        }
        else {
            self.mapOptions.zoomControlOptions = {position: window.google.maps.ControlPosition.RIGHT_TOP};
            self.mapOptions.rotateControlOptions = {position: window.google.maps.ControlPosition.LEFT_TOP};
            self.addMap();
            self.chekcProps();
        }
    }

    componentDidUpdate(prevProps) {
        let coordinates = {};

        if (this.hasOwnProperty("lockbox0")) {
            this.lockbox0.marker.setDraggable( this.props.editLocations);
            if (!this.props.editLocations && prevProps.editLocations) {
                coordinates.lockbox = this.getPosition("lockbox0");
            }
        }
        else if (this.props.editLocations && !prevProps.editLocations){
            let icon = {
                url: "data:image/svg+xml,%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 48.1 48.1' style='enable-background:new 0 0 48.1 48.1;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0%7Bfill:%23CC0000;%7D .st1%7Bfill:%23FFFFFF;%7D%0A%3C/style%3E%3Cg%3E%3Cg%3E%3Cg%3E%3Cline class='st0' x1='-1.5' y1='49.3' x2='49.8' y2='49.3'/%3E%3Cpath class='st0' d='M42.6,18.5c0,9.6-15.4,26.3-17.1,28.1l-1.4,1.5l-1.4-1.5C20.9,44.8,5.5,28.2,5.5,18.5'/%3E%3Cpath class='st0' d='M24.1,42.5c5.1-5.7,14.7-17.7,14.7-24c0-8.1-6.6-14.7-14.7-14.7C16,3.8,9.4,10.4,9.4,18.5 C9.4,24.8,19,36.8,24.1,42.5z'/%3E%3C/g%3E%3Cpath class='st1' d='M24.1,0C13.9,0,5.5,8.3,5.5,18.5c0,9.6,15.4,26.3,17.1,28.1l1.4,1.5l1.4-1.5c1.8-1.9,17.1-18.5,17.1-28.1 C42.6,8.3,34.3,0,24.1,0z M24.1,42.5C19,36.8,9.4,24.8,9.4,18.5c0-8.1,6.6-14.7,14.7-14.7c8.1,0,14.7,6.6,14.7,14.7 C38.7,24.8,29.2,36.8,24.1,42.5z'/%3E%3Cpath class='st1' d='M24.1,10.9c-4.2,0-7.7,3.4-7.7,7.7s3.4,7.7,7.7,7.7s7.7-3.4,7.7-7.7C31.7,14.3,28.3,10.9,24.1,10.9z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A",
                size: new window.google.maps.Size(42, 42),
                scaledSize: new window.google.maps.Size(42, 42),
            };
            this.addPoint("lockbox0", icon, this.props.center);
        }

        if (this.hasOwnProperty("dropzone0")) {
            this.dropzone0.marker.setDraggable( this.props.editDropzone);
            if (!this.props.editDropzone && prevProps.editDropzone) {
                coordinates.dropzone = this.getPosition("dropzone0");
            }
        }
        else if (this.props.editDropzone && !prevProps.editDropzone){
            let icon = {
                url: "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 48.1 48.1' style='enable-background:new 0 0 48.1 48.1;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0%7Bfill:%23CC0000;%7D .st1%7Bfill:%23FFFFFF;%7D%0A%3C/style%3E%3Cg%3E%3Cg%3E%3Ccircle class='st0' cx='24' cy='24.1' r='22.3'/%3E%3Cg%3E%3Cg%3E%3Cpath class='st1' d='M18.8,18.8h-7.3c-0.1,0-0.1,0.1-0.1,0.2c0.2,0.7,0.9,1.3,1.4,1.3h5.1c0.6,0,1-0.6,1.1-1.3 C18.9,18.9,18.8,18.8,18.8,18.8z M12.9,20c-0.4,0-0.8-0.4-1-0.9c0-0.1,0-0.1,0.1-0.1h0.1C12.2,19.5,12.6,19.9,12.9,20L12.9,20z' /%3E%3Cpath class='st1' d='M14.3,16c-0.1-0.5-0.5-0.8-0.9-0.8c-0.4,0-0.8,0.4-0.9,0.8L6,16.1l0.4,0.3c0,0,4.9,0.8,5.2,0.8 c0.3,0,1-0.8,1.1-0.8H14c0,0,0.8,0.8,1.1,0.8c0.3,0,5.2-0.8,5.2-0.8l0.4-0.3L14.3,16z'/%3E%3Cpath class='st1' d='M35.6,16c-0.1-0.5-0.5-0.8-0.9-0.8c-0.4,0-0.8,0.4-0.9,0.8l-6.4,0.1l0.4,0.3c0,0,4.9,0.8,5.2,0.8 c0.3,0,1-0.8,1.1-0.8h1.3c0,0,0.8,0.8,1.1,0.8c0.3,0,5.2-0.8,5.2-0.8l0.4-0.3L35.6,16z'/%3E%3Cpath class='st1' d='M26.2,25.2c0-0.1-0.1-0.2-0.2-0.2h-1.4v-0.9h-0.7h0h-0.7V25h-1.4c-0.1,0-0.2,0.1-0.2,0.2v2.9 c0,0.1,0.1,0.2,0.2,0.2h4.2c0.1,0,0.2-0.1,0.2-0.2v-0.5h0.2v-1.8h-0.2V25.2z M23.8,28.1c-0.8,0-1.4-0.6-1.4-1.4s0.6-1.4,1.4-1.4 c0.8,0,1.4,0.6,1.4,1.4S24.6,28.1,23.8,28.1z'/%3E%3Cpath class='st1' d='M27,25.5h-0.2c-0.2,0-0.3,0.1-0.3,0.3v1.9c0,0.2,0.1,0.3,0.3,0.3H27c0.2,0,0.3-0.1,0.3-0.3v-1.9 C27.3,25.6,27.2,25.5,27,25.5z'/%3E%3Cpath class='st1' d='M21.1,25.8h-0.2c-0.3,0-0.6,0.2-0.6,0.6V27c0,0.3,0.2,0.6,0.6,0.6h0.2c0.1,0,0.2-0.1,0.2-0.2V26 C21.3,25.9,21.2,25.8,21.1,25.8z'/%3E%3Cpath class='st1' d='M17.8,29.3h-0.4c-0.4,0-0.7,0.3-0.7,0.7v0.9c0,0.1,0.1,0.1,0.1,0.1h1.4c0.1,0,0.1-0.1,0.1-0.1v-0.9 C18.4,29.6,18.1,29.3,17.8,29.3z'/%3E%3Cpath class='st1' d='M30.3,29.3h-0.4c-0.4,0-0.7,0.3-0.7,0.7v0.9c0,0.1,0.1,0.1,0.1,0.1h1.4c0.1,0,0.1-0.1,0.1-0.1v-0.9 C30.9,29.6,30.6,29.3,30.3,29.3z'/%3E%3C/g%3E%3Cg%3E%3Cpath class='st1' d='M23.8,26.4c-0.1,0-0.2,0.1-0.2,0.2c0,0.1,0.1,0.2,0.2,0.2c0.1,0,0.2-0.1,0.2-0.2 C24.1,26.5,24,26.4,23.8,26.4z'/%3E%3Cpath class='st1' d='M24.8,26.1c0,0.1-0.1,0.2-0.3,0.2c-0.2,0-0.3-0.1-0.3-0.3c0-0.1,0.1-0.2,0.2-0.3c-0.2-0.1-0.4-0.2-0.6-0.2 c-0.6,0-1.1,0.5-1.1,1.1c0,0.6,0.5,1.1,1.1,1.1c0.6,0,1.1-0.5,1.1-1.1C24.9,26.5,24.9,26.3,24.8,26.1z M23.8,27.2 c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5C24.3,26.9,24.1,27.2,23.8,27.2z'/%3E%3C/g%3E%3Cpath class='st1' d='M14.2,16.6h-1.7c-0.2,0-0.3,0.1-0.3,0.3V19h2.3v-2.1C14.5,16.7,14.4,16.6,14.2,16.6z M12.9,18.8h-0.2v-2h0.2 V18.8z'/%3E%3Cpath class='st1' d='M29.1,18.9c0,0.7,0.5,1.3,1.1,1.3h5.1c0.5,0,1.1-0.6,1.4-1.3c0-0.1,0-0.2-0.1-0.2h-7.3 C29.1,18.8,29.1,18.9,29.1,18.9z M35.1,20c0.3-0.1,0.6-0.5,0.8-1H36c0.1,0,0.1,0.1,0.1,0.1C35.9,19.6,35.5,20,35.1,20L35.1,20z' /%3E%3Cpath class='st1' d='M33.5,16.9V19h2.3v-2.1c0-0.2-0.1-0.3-0.3-0.3h-1.7C33.6,16.6,33.5,16.7,33.5,16.9z M35.1,16.8h0.2v2h-0.2 V16.8z'/%3E%3Cg%3E%3Cpath class='st1' d='M22.8,23.8v0.8c0,0.1,0.1,0.1,0.1,0.1h1.9c0.1,0,0.1-0.1,0.1-0.1v-0.8H22.8z'/%3E%3Cpath class='st1' d='M24.9,23.6v-0.2c0-0.1-0.1-0.1-0.1-0.1h-1.9c-0.1,0-0.1,0.1-0.1,0.1v0.2H24.9z'/%3E%3C/g%3E%3Cpolygon class='st1' points='20.5,22.9 18.6,23.8 19.1,21.3 20.5,21.6 '/%3E%3Cpolygon class='st1' points='27.1,22.9 29,23.8 28.5,21.3 27.1,21.6 '/%3E%3Cpath class='st1' d='M33.7,19.9c0.2-0.2,0.3-0.4,0.3-0.6c0-1.3-4.4-1.7-9.9-1.7c-5.5,0-9.9,0.4-9.9,1.7c0,0.2,0.1,0.4,0.3,0.5 c-0.2,0-0.3,0-0.3,0.1l7,3.3c0.2,0.2,0.8,0.2,1,0.2h1.5h0.3H24h2c0.3,0,0.6-0.1,0.8-0.2l7-3.3C33.9,20,33.8,20,33.7,19.9z M25.6,22.6h-1.8h-0.1H22l-0.6-0.2h2.4h0.1h2.4L25.6,22.6z M24,18.4c-2,0-4.6,0-6.7,0.3c1.8-0.4,4.5-0.5,6.7-0.5v0c0,0,0,0,0,0 c0,0,0,0,0,0v0c2.2,0,4.8,0.1,6.7,0.5C28.6,18.4,26,18.4,24,18.4z'/%3E%3Cpath class='st1' d='M29,20c-0.2-0.2-0.9-0.3-1.1-0.1c-0.2,0.2-0.6,0.5-0.4,0.7c0.8,1.1,2.3,5.1,2,9.6c0,0.3,0.2,0.5,0.5,0.6 c0,0,0,0,0,0c0.3,0,0.5-0.2,0.5-0.5C30.9,25.6,30,21.4,29,20z M30.3,27.7c-0.1-1.3-0.5-3.4-0.8-4.5l0-1.1 C29.8,23.4,30.4,26.4,30.3,27.7z'/%3E%3Cpath class='st1' d='M19.7,19.9c-0.2-0.2-0.9-0.1-1.1,0.1c-1,1.4-1.9,5.7-1.6,10.3c0,0.3,0.3,0.5,0.5,0.5c0,0,0,0,0,0 c0.3,0,0.5-0.3,0.5-0.6c-0.3-4.5,1.3-8.5,2-9.6C20.3,20.3,20,20,19.7,19.9z M18.2,23.2c-0.4,1.1-0.8,3.2-0.8,4.5 c-0.1-1.3,0.5-4.4,0.9-5.6L18.2,23.2z'/%3E%3Cellipse class='st1' cx='24' cy='33' rx='11.2' ry='1.1'/%3E%3C/g%3E%3Cpath class='st1' d='M24,47.9c-13.1,0-23.8-10.7-23.8-23.8C0.2,10.9,10.9,0.2,24,0.2s23.8,10.7,23.8,23.8 C47.8,37.2,37.1,47.9,24,47.9z M24,3.2c-11.5,0-20.8,9.3-20.8,20.8S12.5,44.9,24,44.9c11.5,0,20.8-9.3,20.8-20.8S35.5,3.2,24,3.2z '/%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A",
                size: new window.google.maps.Size(42, 42),
                scaledSize: new window.google.maps.Size(42, 42),
                anchor: new window.google.maps.Point(21,21)
            };
            this.addPoint("dropzone0", icon, this.props.center);
        }

        if (!this.props.editLocations && prevProps.editLocations) {
            this.props.setLockboxCoordinates(coordinates);
        }

        if (!this.props.editDropzone && prevProps.editDropzone) {
            this.props.setDropzoneCoordinates(coordinates);
        }

        if (this.props.mapType !== prevProps.mapType) {
            this.map.setMapTypeId(window.google.maps.MapTypeId[this.props.mapType]);
        }

    }

    addMap() {
        let self = this;
        /**
         * Add map to the doom and make it global in this component
         * @type {window.googleMapsAPI.Map}
         */
        console.log(self.props.center);
        self.map = new window.google.maps.Map(document.getElementById("map-canvas"), self.mapOptions);
    }

    chekcProps() {
        let self = this;
        /**
         * Check lockboxes object and send to handle
         */
        if (typeof(self.props.lockboxes) === "object") {
            console.log("handle lockboxes");
            self.handleLockboxes(self.props.lockboxes);
        }

        /**
         * Check landingzones object and send to handle
         */
        if (typeof(self.props.langingzones) === "object") {
            self.handleLangingzones(self.props.langingzones);
        }
    }

    /**
     * Handle lockboxes
     * @param boxes
     */
    handleLockboxes(boxes){
        let self = this,
            icon = {
                url: "data:image/svg+xml,%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 48.1 48.1' style='enable-background:new 0 0 48.1 48.1;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0%7Bfill:%23CC0000;%7D .st1%7Bfill:%23FFFFFF;%7D%0A%3C/style%3E%3Cg%3E%3Cg%3E%3Cg%3E%3Cline class='st0' x1='-1.5' y1='49.3' x2='49.8' y2='49.3'/%3E%3Cpath class='st0' d='M42.6,18.5c0,9.6-15.4,26.3-17.1,28.1l-1.4,1.5l-1.4-1.5C20.9,44.8,5.5,28.2,5.5,18.5'/%3E%3Cpath class='st0' d='M24.1,42.5c5.1-5.7,14.7-17.7,14.7-24c0-8.1-6.6-14.7-14.7-14.7C16,3.8,9.4,10.4,9.4,18.5 C9.4,24.8,19,36.8,24.1,42.5z'/%3E%3C/g%3E%3Cpath class='st1' d='M24.1,0C13.9,0,5.5,8.3,5.5,18.5c0,9.6,15.4,26.3,17.1,28.1l1.4,1.5l1.4-1.5c1.8-1.9,17.1-18.5,17.1-28.1 C42.6,8.3,34.3,0,24.1,0z M24.1,42.5C19,36.8,9.4,24.8,9.4,18.5c0-8.1,6.6-14.7,14.7-14.7c8.1,0,14.7,6.6,14.7,14.7 C38.7,24.8,29.2,36.8,24.1,42.5z'/%3E%3Cpath class='st1' d='M24.1,10.9c-4.2,0-7.7,3.4-7.7,7.7s3.4,7.7,7.7,7.7s7.7-3.4,7.7-7.7C31.7,14.3,28.3,10.9,24.1,10.9z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A",
                size: new window.google.maps.Size(42, 42),
                scaledSize: new window.google.maps.Size(42, 42),
            };

        for (let key in boxes) {
            if (boxes[key] === undefined) { return; }
            if (boxes[key].hasOwnProperty("lat") && boxes[key].hasOwnProperty("lng")) {
                self.addPoint("lockbox" + key,icon, boxes[key]);
            }
        }
    }

    /**
     * Handle landing zones
     * @param zones
     */
    handleLangingzones(zones) {
        let self = this,
            icon = {
                url: "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!-- Generator: Adobe Illustrator 22.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 48.1 48.1' style='enable-background:new 0 0 48.1 48.1;' xml:space='preserve'%3E%3Cstyle type='text/css'%3E .st0%7Bfill:%23CC0000;%7D .st1%7Bfill:%23FFFFFF;%7D%0A%3C/style%3E%3Cg%3E%3Cg%3E%3Ccircle class='st0' cx='24' cy='24.1' r='22.3'/%3E%3Cg%3E%3Cg%3E%3Cpath class='st1' d='M18.8,18.8h-7.3c-0.1,0-0.1,0.1-0.1,0.2c0.2,0.7,0.9,1.3,1.4,1.3h5.1c0.6,0,1-0.6,1.1-1.3 C18.9,18.9,18.8,18.8,18.8,18.8z M12.9,20c-0.4,0-0.8-0.4-1-0.9c0-0.1,0-0.1,0.1-0.1h0.1C12.2,19.5,12.6,19.9,12.9,20L12.9,20z' /%3E%3Cpath class='st1' d='M14.3,16c-0.1-0.5-0.5-0.8-0.9-0.8c-0.4,0-0.8,0.4-0.9,0.8L6,16.1l0.4,0.3c0,0,4.9,0.8,5.2,0.8 c0.3,0,1-0.8,1.1-0.8H14c0,0,0.8,0.8,1.1,0.8c0.3,0,5.2-0.8,5.2-0.8l0.4-0.3L14.3,16z'/%3E%3Cpath class='st1' d='M35.6,16c-0.1-0.5-0.5-0.8-0.9-0.8c-0.4,0-0.8,0.4-0.9,0.8l-6.4,0.1l0.4,0.3c0,0,4.9,0.8,5.2,0.8 c0.3,0,1-0.8,1.1-0.8h1.3c0,0,0.8,0.8,1.1,0.8c0.3,0,5.2-0.8,5.2-0.8l0.4-0.3L35.6,16z'/%3E%3Cpath class='st1' d='M26.2,25.2c0-0.1-0.1-0.2-0.2-0.2h-1.4v-0.9h-0.7h0h-0.7V25h-1.4c-0.1,0-0.2,0.1-0.2,0.2v2.9 c0,0.1,0.1,0.2,0.2,0.2h4.2c0.1,0,0.2-0.1,0.2-0.2v-0.5h0.2v-1.8h-0.2V25.2z M23.8,28.1c-0.8,0-1.4-0.6-1.4-1.4s0.6-1.4,1.4-1.4 c0.8,0,1.4,0.6,1.4,1.4S24.6,28.1,23.8,28.1z'/%3E%3Cpath class='st1' d='M27,25.5h-0.2c-0.2,0-0.3,0.1-0.3,0.3v1.9c0,0.2,0.1,0.3,0.3,0.3H27c0.2,0,0.3-0.1,0.3-0.3v-1.9 C27.3,25.6,27.2,25.5,27,25.5z'/%3E%3Cpath class='st1' d='M21.1,25.8h-0.2c-0.3,0-0.6,0.2-0.6,0.6V27c0,0.3,0.2,0.6,0.6,0.6h0.2c0.1,0,0.2-0.1,0.2-0.2V26 C21.3,25.9,21.2,25.8,21.1,25.8z'/%3E%3Cpath class='st1' d='M17.8,29.3h-0.4c-0.4,0-0.7,0.3-0.7,0.7v0.9c0,0.1,0.1,0.1,0.1,0.1h1.4c0.1,0,0.1-0.1,0.1-0.1v-0.9 C18.4,29.6,18.1,29.3,17.8,29.3z'/%3E%3Cpath class='st1' d='M30.3,29.3h-0.4c-0.4,0-0.7,0.3-0.7,0.7v0.9c0,0.1,0.1,0.1,0.1,0.1h1.4c0.1,0,0.1-0.1,0.1-0.1v-0.9 C30.9,29.6,30.6,29.3,30.3,29.3z'/%3E%3C/g%3E%3Cg%3E%3Cpath class='st1' d='M23.8,26.4c-0.1,0-0.2,0.1-0.2,0.2c0,0.1,0.1,0.2,0.2,0.2c0.1,0,0.2-0.1,0.2-0.2 C24.1,26.5,24,26.4,23.8,26.4z'/%3E%3Cpath class='st1' d='M24.8,26.1c0,0.1-0.1,0.2-0.3,0.2c-0.2,0-0.3-0.1-0.3-0.3c0-0.1,0.1-0.2,0.2-0.3c-0.2-0.1-0.4-0.2-0.6-0.2 c-0.6,0-1.1,0.5-1.1,1.1c0,0.6,0.5,1.1,1.1,1.1c0.6,0,1.1-0.5,1.1-1.1C24.9,26.5,24.9,26.3,24.8,26.1z M23.8,27.2 c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5C24.3,26.9,24.1,27.2,23.8,27.2z'/%3E%3C/g%3E%3Cpath class='st1' d='M14.2,16.6h-1.7c-0.2,0-0.3,0.1-0.3,0.3V19h2.3v-2.1C14.5,16.7,14.4,16.6,14.2,16.6z M12.9,18.8h-0.2v-2h0.2 V18.8z'/%3E%3Cpath class='st1' d='M29.1,18.9c0,0.7,0.5,1.3,1.1,1.3h5.1c0.5,0,1.1-0.6,1.4-1.3c0-0.1,0-0.2-0.1-0.2h-7.3 C29.1,18.8,29.1,18.9,29.1,18.9z M35.1,20c0.3-0.1,0.6-0.5,0.8-1H36c0.1,0,0.1,0.1,0.1,0.1C35.9,19.6,35.5,20,35.1,20L35.1,20z' /%3E%3Cpath class='st1' d='M33.5,16.9V19h2.3v-2.1c0-0.2-0.1-0.3-0.3-0.3h-1.7C33.6,16.6,33.5,16.7,33.5,16.9z M35.1,16.8h0.2v2h-0.2 V16.8z'/%3E%3Cg%3E%3Cpath class='st1' d='M22.8,23.8v0.8c0,0.1,0.1,0.1,0.1,0.1h1.9c0.1,0,0.1-0.1,0.1-0.1v-0.8H22.8z'/%3E%3Cpath class='st1' d='M24.9,23.6v-0.2c0-0.1-0.1-0.1-0.1-0.1h-1.9c-0.1,0-0.1,0.1-0.1,0.1v0.2H24.9z'/%3E%3C/g%3E%3Cpolygon class='st1' points='20.5,22.9 18.6,23.8 19.1,21.3 20.5,21.6 '/%3E%3Cpolygon class='st1' points='27.1,22.9 29,23.8 28.5,21.3 27.1,21.6 '/%3E%3Cpath class='st1' d='M33.7,19.9c0.2-0.2,0.3-0.4,0.3-0.6c0-1.3-4.4-1.7-9.9-1.7c-5.5,0-9.9,0.4-9.9,1.7c0,0.2,0.1,0.4,0.3,0.5 c-0.2,0-0.3,0-0.3,0.1l7,3.3c0.2,0.2,0.8,0.2,1,0.2h1.5h0.3H24h2c0.3,0,0.6-0.1,0.8-0.2l7-3.3C33.9,20,33.8,20,33.7,19.9z M25.6,22.6h-1.8h-0.1H22l-0.6-0.2h2.4h0.1h2.4L25.6,22.6z M24,18.4c-2,0-4.6,0-6.7,0.3c1.8-0.4,4.5-0.5,6.7-0.5v0c0,0,0,0,0,0 c0,0,0,0,0,0v0c2.2,0,4.8,0.1,6.7,0.5C28.6,18.4,26,18.4,24,18.4z'/%3E%3Cpath class='st1' d='M29,20c-0.2-0.2-0.9-0.3-1.1-0.1c-0.2,0.2-0.6,0.5-0.4,0.7c0.8,1.1,2.3,5.1,2,9.6c0,0.3,0.2,0.5,0.5,0.6 c0,0,0,0,0,0c0.3,0,0.5-0.2,0.5-0.5C30.9,25.6,30,21.4,29,20z M30.3,27.7c-0.1-1.3-0.5-3.4-0.8-4.5l0-1.1 C29.8,23.4,30.4,26.4,30.3,27.7z'/%3E%3Cpath class='st1' d='M19.7,19.9c-0.2-0.2-0.9-0.1-1.1,0.1c-1,1.4-1.9,5.7-1.6,10.3c0,0.3,0.3,0.5,0.5,0.5c0,0,0,0,0,0 c0.3,0,0.5-0.3,0.5-0.6c-0.3-4.5,1.3-8.5,2-9.6C20.3,20.3,20,20,19.7,19.9z M18.2,23.2c-0.4,1.1-0.8,3.2-0.8,4.5 c-0.1-1.3,0.5-4.4,0.9-5.6L18.2,23.2z'/%3E%3Cellipse class='st1' cx='24' cy='33' rx='11.2' ry='1.1'/%3E%3C/g%3E%3Cpath class='st1' d='M24,47.9c-13.1,0-23.8-10.7-23.8-23.8C0.2,10.9,10.9,0.2,24,0.2s23.8,10.7,23.8,23.8 C47.8,37.2,37.1,47.9,24,47.9z M24,3.2c-11.5,0-20.8,9.3-20.8,20.8S12.5,44.9,24,44.9c11.5,0,20.8-9.3,20.8-20.8S35.5,3.2,24,3.2z '/%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A",
                size: new window.google.maps.Size(42, 42),
                scaledSize: new window.google.maps.Size(42, 42),
                anchor: new window.google.maps.Point(21,21)
            };

        for (let key in zones) {
            if (zones[key] === undefined) { return; }
            if (zones[key].hasOwnProperty("lat") && zones[key].hasOwnProperty("lng")) {
                self.addPoint("dropzone" + key,icon, zones[key]);
            }
        }
    }

    /**
     * Add marker to the map depend on type
     * @param type
     * @param icon
     * @param coord
     */
    addPoint(type , icon, coord) {
        var self = this;

        /**
         * Prevent the type is undefined
         */
        if (!this.hasOwnProperty(type)) {
            self[type] = {};
        }

        let edit = false;

        if (type === "lockbox0") {
            edit = this.props.editLocations;
        }

        if (type === "dropzone0") {
            edit = this.props.editDropzone;
        }

        /**
         * Create the marker and add to the map
         */
        this[type].marker = new window.google.maps.Marker({
            position: coord,
            map: self.map,
            draggable: edit,
            animation: "DROP",
            icon: icon,
            zIndex : -20
        });

        // this.map.setCenter(coord);

        // this.map.panTo(coord);
    }

    /**
     * Return the marker position
     * @param type
     * @returns {{lng: *, lat: *}}
     */
    getPosition(type) {
        let lat = this[type].marker.getPosition().lat(),
            lng = this[type].marker.getPosition().lng();

        return {lat: lat, lng:lng};
    }

    /**
     * Render the component
     * @returns {*}
     */
    render() {
        const mapHolderStyle = {
            position: "absolute",
            top: this.props.top,
            left: this.props.left,
            right: this.props.right,
            bottom: this.props.bottom
        };
        return (<div id="map-canvas" style={mapHolderStyle}/>);
    }
}

Map.propTypes = {
    mapStyle: PropTypes.array,
    center: PropTypes.object,
    lockboxes: PropTypes.array,
    langingzones: PropTypes.array,
    top: PropTypes.string,
    left: PropTypes.string,
    right: PropTypes.string,
    bottom: PropTypes.string,
    setLockboxCoordinates: PropTypes.func,
    setDropzoneCoordinates: PropTypes.func,
    editLocations: PropTypes.bool,
    editDropzone: PropTypes.bool,
    mapType: PropTypes.string,
};

Map.defaultProps = {
    mapStyle: [],
    center: {lat: 0, lng: 0},
    lockboxes: [],
    langingzones: [],
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    editLocations: false,
    editDropzone: false,
    mapType: "SATELLITE"
};