import React, { Component } from "react";
import LoaderService from "./services/Loader";

export default class Loader extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showLoader: LoaderService.getState()
        };

        LoaderService.on("change", () => {
            this.setState({
                showLoader: LoaderService.getState(),
            });
        });
    }

    render() {
        let loaderClassName = "page-loader";
        if (this.state.showLoader) {
            loaderClassName += " active";
        }

        return (
            <div className={loaderClassName}>
                <div className="container">
                    <div className="box1"/>
                    <div className="box2"/>
                    <div className="box3"/>
                </div>
            </div>
        );
    }
}