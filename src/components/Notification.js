import React, { Component } from "react";
import NotificationService from "./services/Notification";

export default class Loader extends Component {

    constructor(props) {
        super(props);

        this.state = NotificationService.getState();
    }

    componentDidMount() {
        NotificationService.on("change", () => {
            this.setState(NotificationService.getState());
        });
    }

    ok(e){
        e.preventDefault();
        NotificationService.hideNotification();
    }

    render() {
        let loaderClassName = "notification error";
        if (this.state.showNotification) {
            loaderClassName += " active";
        }

        return (
            <div className={loaderClassName}>
                <div className="notification-window">
                    <div className="notification-text">{this.state.text}</div>
                    <button type="button" onClick={this.ok}>
                        <span>Ok</span>
                    </button>
                </div>
            </div>
        );
    }
}