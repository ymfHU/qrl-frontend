import React from "react";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";
import Select from "react-validation/build/select";

import PageHandlerStore from "../stores/PageHandler";
import ValidationService from "./services/Validation";

import IndicatorStore from "../stores/Indicator";
import RegistrationDataStore from "../stores/RegistrationData";
import CitiesStore from "../stores/Cities";
import UserDataStore from "../stores/UserData";
import {browserHistory} from "react-router";

export default  class Edit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cities: [],
            states:CitiesStore.getStates(),
            formdata: RegistrationDataStore.getDataAll()
        };

        this.next = this.next.bind(this);
    }

    componentDidMount() {
        if (UserDataStore.getData("token") === "") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("/");
            return;
        }
        IndicatorStore.setVariable("indicator", [
            {
                label: "Personal data",
                count: 1,
                status: "finished"
            },
            {
                label: "QR Code",
                count: 2,
                status: "finished"
            },
            {
                label: "Location",
                count: 3,
                status: "active"
            }
        ]);

        RegistrationDataStore.on("change", () => {
            this.setState({formdata: RegistrationDataStore.getDataAll()});
        });

        PageHandlerStore.setVariable("title","Registration");
        PageHandlerStore.setVariable("subtitle","Set your location");
    }

    stateChange(e) {
        RegistrationDataStore.setData(e.target.name, e.target.value);
        RegistrationDataStore.setData("city", "");
        this.setState({cities: CitiesStore.getCities(e.target.value)});
    }

    onChange(e) {
        RegistrationDataStore.setData(e.target.name, e.target.value);
    }

    next(e){
        e.preventDefault();
        // PageHandlerStore.setVariable("goToPage","/registration/step6");
        browserHistory.push("/registration/step6");
    }

    render()
    {
        return (
            <div className="content">
                <div className="wrapper">
                    <Form>
                        <div className="row">
                            <div className="col-xs-12 text-center text-italic text-small">At this time we accept US address only</div>
                            <div className="col-xs-12">
                                <Select name="state" className="full-width" value={this.state.formdata.state} validations={[ValidationService.required]} onChange={this.stateChange.bind(this)}>
                                    <option value="">Choose your state, provence</option>
                                    {this.state.states.map((item, i) => {
                                        return (
                                            <option value={item} key={i}>{item}</option>
                                        );
                                    })}
                                </Select>
                            </div>
                            <div className="col-xs-12">
                                <Select name="city" className="full-width" value={this.state.formdata.city} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}>
                                    <option value="">Choose your city, town</option>
                                    {this.state.cities.map((item, i) => {
                                        return (
                                            <option value={item} key={i}>{item}</option>
                                        );
                                    })}
                                </Select>
                            </div>
                            <div className="col-xs-12">
                                <Input className="full-width" placeholder="address*" name="address" value={this.state.formdata.address} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                            </div>
                            <div className="col-xs-12">
                                <Input className="full-width" placeholder="zip code*" name="zip"value={this.state.formdata.zip} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-6">
                                <button type="button" className="full-width" onClick={() => {browserHistory.push("/registration/step4");}}>
                                    <span>Back</span>
                                </button>
                            </div>
                            <div className="col-xs-6">
                                <Button className="full-width" onClick={this.next}>
                                    <span>Next</span>
                                </Button>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}