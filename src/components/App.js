import React, { Component } from "react";
// import {Route, Switch} from "react-router-dom";
// import {
//     CSSTransition,
//     TransitionGroup,
// } from "react-transition-group";

import Login from "./pages/Login";
import RegStep1 from "./registration/Step1";
import RegStep2 from "./registration/Step2";
import RegStep3 from "./registration/Step3";
import RegStep4 from "./registration/Step4";
import RegStep5 from "./registration/Step5";
import RegStep6 from "./registration/Step6";
import RegFinish from "./registration/Finish";
import MyLocations from "./pages/MyLocations";
import Details from "./pages/Details";

import About from "./pages/About";
// import Error from "./Error";
import Menu from "./header/Menu_btn";
import Header from "./header/Header";
// import PageHandler from "./services/PageHandler";
import Loader from "./Loader";
import Notification from "./Notification";
import IndicatorStore from "../stores/Indicator";
import PageHandlerStore from "../stores/PageHandler";


import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Router, browserHistory} from "react-router";
import reducers from "./reducers";
import loadGoogleMapsApi from "load-google-maps-api";
const createStoreWithMiddleware = applyMiddleware()(createStore);

// import Scroll from "./Scroll";


// import IScroll from "iscroll-react";


// import iScroll from "iscroll";
// import ReactIScroll from "react-iscroll";


// import iScroll from "iscroll";
//
// import ReactIScroll from "react-iscroll";

// import IScroll from "iscroll-react";
// import iscroll from "iscroll";

const routes =  [
    { path: "/login", component: Login },
    { path: "/about", component: About },
    { path: "/registration/step1", component: RegStep1 },
    { path: "/registration/step2", component: RegStep2 },
    { path: "/registration/step3", component: RegStep3 },
    { path: "/registration/step4", component: RegStep4 },
    { path: "/registration/step5", component: RegStep5 },
    { path: "/registration/step6", component: RegStep6 },
    { path: "/registration/finish", component: RegFinish },
    { path: "/my-locations", component: MyLocations },
    { path: "/details", component: Details },
    { path: "/edit", component: RegStep5 },
    { path: "/review", component: RegStep6 },
    { path: "/scan", component: RegStep3 },
    { path: "/add-location/step1", component: RegStep3 },
    { path: "/add-location/step2", component: RegStep4 },
    { path: "/add-location/step3", component: RegStep5 },
    { path: "/add-location/step4", component: RegStep6 },
    { path: "/add-location/finish", component: RegFinish },
    { path: "*", component: Login}
];

window.qrLocation = {
    mapColors: {
        false: [
            {
                featureType: "poi.business",
                elementType: "labels",
                stylers    : [{
                    visibility: "off"
                }]
            },
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dadada"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c9c9c9"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            }
        ],
        true: [
            {
                featureType: "poi.business",
                elementType: "labels",
                stylers    : [{
                    visibility: "off"
                }]
            },
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#4c4f4f"
                    },
                    {
                        "visibility": "on"
                    },
                    {
                        "weight": 3
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "on"
                    },
                    {
                        "weight": 3
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#242424"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#131313"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#181818"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#1b1b1b"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#2c2c2c"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#8a8a8a"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#373737"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#3c3c3c"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#4e4e4e"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#1a3880"
                    },
                    {
                        "saturation": -55
                    },
                    {
                        "lightness": 50
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#3d3d3d"
                    }
                ]
            }
        ]
    },
    colors: {
        false: {
            "--color-background"      : "#e1e1e1",
            "--color-background-rgb"  : "225,225,225",
            "--color-background2"     : "#d9d9d9",
            "--color-background2-rgb" : "217,217,217",
            "--color-header"          : "#CC0000",
            "--color-header-border"   : "#4f4f4f",
            "--color-header-after-bg" : "#f0f1f1",
            "--color-decorator-line"  : "#d6d6d6",
            "--color-databox-bg"      : "#ffffff",
            "--color-databox-font"    : "#000000",
            "--color-font-default"    : "#4f4f4f",
            "--color-font-highlighted": "#000000",

            "--color-input-text"          : "#4f4f4f",
            "--color-input-bg"            : "unset",
            "--color-input-border"        : "#4f4f4f",
            "--color-input-focus-text"    : "#000000",
            "--color-input-focus-bg"      : "unset",
            "--color-input-focus-border"  : "#000000",
            "--color-inpud-selection-text": "#ff0000",
            "--color-inpud-selection-bg"  : "#4f4f4f",
            "--color-input-error-text"    : "#ff0000",
            "--color-input-error-text-rgb": "255,0,0",

            "--color-button-text"        : "#161616",
            "--color-button-bg"          : "unset",
            "--color-button-border"      : "#161616",
            "--color-button-hover-text"  : "#ffffff",
            "--color-button-hover-bg"    : "#CC0000",
            "--color-button-hover-bg-rgb": "204,0,0",
            "--color-button-hover-border": "#CC0000",
        },
        true : {
            "--color-background"      : "#000000",
            "--color-background-rgb"      : "0,0,0",
            "--color-background2"     : "#040404",
            "--color-background2-rgb" : "4,4,4",
            "--color-header"          : "#CC0000",
            "--color-header-border"   : "#ffffff",
            "--color-header-after-bg" : "#403939",
            "--color-decorator-line"  : "#161616",
            "--color-databox-bg"      : "#000000",
            "--color-databox-font"    : "#ffffff",
            "--color-font-default"    : "#AFAFAF",
            "--color-font-highlighted": "#ffffff",

            "--color-input-text"          : "#AFAFAF",
            "--color-input-bg"            : "unset",
            "--color-input-border"        : "#AFAFAF",
            "--color-input-focus-text"    : "#FFFFFF",
            "--color-input-focus-bg"      : "unset",
            "--color-input-focus-border"  : "#FFFFFF",
            "--color-inpud-selection-text": "#ff0000",
            "--color-inpud-selection-bg"  : "#ffffff",
            "--color-input-error-text"    : "#ff0000",
            "--color-input-error-text-rgb": "255,0,0",
            "--color-button-text"         : "#AFAFAF",
            "--color-button-bg"           : "unset",
            "--color-button-border"       : "#AFAFAF",
            "--color-button-hover-text"   : "#ffffff",
            "--color-button-hover-bg"     : "#CC0000",
            "--color-button-hover-bg-rgb" : "204,0,0",
            "--color-button-hover-border" : "#CC0000",
        }
    }
};

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isSplashHide: true,
            isDarkMode: PageHandlerStore.getValue("isDarkMode"),
            indicator: IndicatorStore.getValue("indicator"),
            subtitle: PageHandlerStore.getValue("subtitle"),
        };
    }

    componentDidMount() {
        /**
         * Load Google Maps API
         */
        loadGoogleMapsApi({key:"AIzaSyCuv4pnWSAuoNZLUQZnd6REItQVmLJmEpg&libraries=places"}).then(function (googleMaps) {
            window.google.maps = googleMaps;
        });

        IndicatorStore.on("change", () => {
            this.setState({
                indicator: IndicatorStore.getValue("indicator"),
            });
        });

        PageHandlerStore.on("change", () => {
            this.setState({
                subtitle: PageHandlerStore.getValue("subtitle"),
                isDarkMode: PageHandlerStore.getValue("isDarkMode"),
            });
            this.setTheme();
        });

        this.setTheme();
    }

    componentWillUnmount() {
    }

    isCordova() {
        return document.location.protocol === "file:";
    }

    setTheme() {
        if (this.isCordova()) {
            window.plugins.webviewcolor.change(window.qrLocation.colors[this.state.isDarkMode]["--color-background"]);
        }

        for (let key in window.qrLocation.colors[this.state.isDarkMode]) {
            document.querySelector("body").style.setProperty(key, window.qrLocation.colors[this.state.isDarkMode][key]);
        }

        if (this.isCordova() && this.state.isSplashHide) {
            navigator.splashscreen.hide();
            this.setState({isSplashHide: false});
        }
    }

    getMainClass() {
        var _classes = "";

        if (this.state.indicator.length > 0) {
            _classes +="indicator-on";
        }

        if (this.state.subtitle.length > 0) {
            _classes +=" subtitle-on";
        }

        return _classes;
    }

    render() {
        return (
            <div id="app-wrapper" className={"isDarlmode" + this.state.isDarkMode}>
                <Notification />
                <Loader />
                <Menu />
                <Header />
                <main className={this.getMainClass()}>
                    <Provider store={createStoreWithMiddleware(reducers)}>
                        <Router history={browserHistory} routes={routes}/>
                    </Provider>
                </main>
            </div>
        );
    }
}