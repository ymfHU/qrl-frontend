import React from "react";

import Scanner from "../Scanner";

import LoaderService from "../services/Loader";

import PageHandlerStore from "../../stores/PageHandler";
import IndicatorStore from "../../stores/Indicator";
import ScannerDataStore from "../../stores/ScannerData";
import QRDataStore from "../../stores/QRdata";
import NotificationService from "../services/Notification";
import UserDataStore from "../../stores/UserData";
import RegistrationDataStore from "../../stores/RegistrationData";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import ValidationService from "../services/Validation";
import {browserHistory} from "react-router";

import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";

// import Draggable from "../Draggable";

// import Map from "../map/Map";

export default  class RegStep3 extends React.Component {
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this._isMounted = false;

        this.center = {lat:28.023534, lng:-82.732015};
        this.lockboxes = [
            {lat:28.02353349130747, lng:-82.73209454426109},
            {lat:28.023534, lng:-82.732015}
        ];
        this.landingzones = [
            {lat:28.02353349130647, lng:-82.73209454426209},
            {lat:28.023434, lng:-82.732115}
        ];

        this.state = {
            showScanner: "ready",
            qrCode: "",
            qr_code_manually: "",
            error: false,
            path: window.location.pathname.replace("/","")
        };

        this.scan = this.scan.bind(this);
        this.stopScan = this.stopScan.bind(this);
        this.scanSuccess = this.scanSuccess.bind(this);
        this.addManually = this.addManually.bind(this);
        this.qrAssignSuccess = this.qrAssignSuccess.bind(this);
        this.qrAssignError = this.qrAssignError.bind(this);
        this.back = this.back.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;

        if (this._isMounted && UserDataStore.getData("token") === "") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("/");
            return;
        }
        this._isMounted && QRDataStore.resetData();
        this._isMounted && RegistrationDataStore.setData("address", "");
        this._isMounted && RegistrationDataStore.setData("state", "");
        this._isMounted && RegistrationDataStore.setData("city", "");
        this._isMounted && RegistrationDataStore.setData("zip", "");

        switch ( this.state.path ) {
        case "scan":
            this._isMounted && IndicatorStore.setVariable("indicator", []);
            this._isMounted && PageHandlerStore.setVariable("title","Scan QR Code");
            this._isMounted && PageHandlerStore.setVariable("subtitle","");
            break;
        case "add-location/step1":
            this._isMounted && IndicatorStore.setVariable("indicator", [
                {
                    label: "QR Code",
                    count: 1,
                    status: "active"
                },
                {
                    label: "Location",
                    count: 2,
                    status: "disabled"
                }
            ]);
            this._isMounted && PageHandlerStore.setVariable("title","Add Location");
            this._isMounted && PageHandlerStore.setVariable("subtitle","Scan QR Code");
            break;
        default:
            this._isMounted && IndicatorStore.setVariable("indicator", [
                {
                    label: "Personal data",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "QR Code",
                    count: 2,
                    status: "active"
                },
                {
                    label: "Location",
                    count: 3,
                    status: "disabled"
                }
            ]);

            this._isMounted && PageHandlerStore.setVariable("title","Registration");
            this._isMounted && PageHandlerStore.setVariable("subtitle","Scan QR Code");
            break;
        }

        this.ScannerDataStoreSubscription = this._isMounted && ScannerDataStore.on("change", () => {
            let newQrCode = ScannerDataStore.getData("qrCode");

            if (newQrCode !== "" && newQrCode!== null && newQrCode!== undefined) {
                this._isMounted && this.setState({
                    showScanner: "scanned",
                    qrCode: newQrCode,
                });
                if (this.state.path === "scan") {
                    this._isMounted && LoaderService.getData("courier/qr-code", newQrCode, this.scanSuccess, this.scanError);
                }
                else {
                    this._isMounted && LoaderService.getData("customer/qr-code", newQrCode, this.scanSuccess, this.scanError);
                }
            }
            else if (ScannerDataStore.getData("error")){
                this._isMounted && this.setState({
                    error: true
                });
            }
            else {
                this._isMounted && this.setState({
                    showScanner: "ready",
                    qrCode: "",
                    error: false
                });
            }
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    onQRScannerPrepareDone(err, status){
        if (err) {
            // here we can handle errors and clean up any loose ends.
            alert(err);
        }
        if (status.authorized) {
            // W00t, you have camera access and the scanner is initialized.
            // QRscanner.show() should feel very fast.
        } else if (status.denied) {
            // The video preview will remain black, and scanning is disabled. We can
            // try to ask the user to change their mind, but we'll have to send them
            // to their device settings with `QRScanner.openSettings()`.
        } else {
            // we didn't get permission, but we didn't get permanently denied. (On
            // Android, a denial isn't permanent unless the user checks the "Don't
            // ask again" box.) We can ask again at the next relevant opportunity.
        }
    }

    qrAssignSuccess() {
        if (this.state.path === "add-location/step1") {
            // PageHandlerStore.setVariable("goToPage","/add-location/step2");
            browserHistory.push("/add-location/step2");
        }
        else {
            // PageHandlerStore.setVariable("goToPage","/registration/step4");
            browserHistory.push("/registration/step4");
        }
    }

    qrAssignError() {
        ScannerDataStore.setData("qrCode","");
    }

    scanSuccess(result) {
        if (this._isMounted && result.hasOwnProperty("id")) {
            QRDataStore.setData({
                qrcode: result.qrcode,
                address1: result.address1,
                address2: result.address2,
                boxtype_name: result.boxtype_name,
                city: result.city,
                colorcode_name: result.colorcode_name,
                description: result.description,
                id: result.id,
                keypadtype_name: result.keypadtype_name,
                keypadtype_version: result.keypadtype_version,
                zip: result.zip,
                state: result.state,
                latitude: result.latitude,
                longitude: result.longitude
            });

            if (this.state.path === "scan") {
                // PageHandlerStore.setVariable("goToPage","/details");
                browserHistory.push("/details");
            }
            else {
                let data = {qrcode_id: QRDataStore.getData("id")};
                LoaderService.postData("customer/assign-qr-code", data, this.qrAssignSuccess, this.qrAssignError);
            }
        }
        else {
            //TODO: handle if no id in the response
        }
    }

    scanError(result) {
        console.log(result);
        if (result!== undefined && result!== "" && result.hasOwnProperty("message")) {
            NotificationService.showError(result.status, result.message);
        }
        ScannerDataStore.setData("qrCode","");
    }

    back(e){
        e.preventDefault();
        // PageHandlerStore.setVariable("goToPage","/my-locations");
        browserHistory.push("/my-locations");
    }

    scan(e){
        e.preventDefault();
        let self = this;

        if (window.hasOwnProperty("cordova")) {

            if (typeof cordova === "undefined") {
                var cordova = window.cordova;
            }

            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    if (self._isMounted && !result.cancelled) {
                        ScannerDataStore.setData("qrCode",result.text);
                    }
                },
                function (error) {
                    NotificationService.showError("Scanning failed", error);
                },
                {
                    preferFrontCamera : false, // iOS and Android
                    showFlipCameraButton : true, // iOS and Android
                    showTorchButton : true, // iOS and Android
                    torchOn: true, // Android, launch with the torch switched on (if available)
                    saveHistory: false, // Android, save scan history (default false)
                    prompt : "Place a barcode inside the scan area", // Android
                    resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
                    formats : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
                    orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
                    disableAnimations : true, // iOS
                    disableSuccessBeep: false // iOS and Android
                }
            );
        }
        else {
            this.setState({showScanner: "scan"});
        }
    }

    stopScan(e){
        e.preventDefault();
        this.setState({showScanner: "ready"});
    }

    changeCamera(e){
        e.preventDefault();
        let activeCamera = QRDataStore.getData("facingMode");
        if (activeCamera === "front") {activeCamera = "rear";} else {activeCamera = "front";}
        QRDataStore.setData("facingMode",activeCamera);
    }

    addManually(e){
        e.preventDefault();
        this.setState({error: false});
        ScannerDataStore.setData("error",false);
        ScannerDataStore.setData("qrCode",this.state.qr_code_manually);
        // this.setState({showScanner: "ready"});
    }

    onChange(e) {
        this.setState({qr_code_manually: e.target.value});
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <div>
                            { self.state.showScanner === "scanned" ? <div className="row">
                                <div className="col-xs-12 text-center">Checking the code, please wait...</div>
                            </div> : null }

                            { (self.state.showScanner === "ready" || self.state.showScanner === "scan") && !self.state.error ? <div className="row v-middle">
                                <div className="scanner">
                                    { self.state.showScanner === "scan" ? <Scanner /> : null }
                                    { self.state.showScanner === "ready" ? <div className="col-xs-12 text-center">Press the scan button below and
                                        direct the camera to the QR code
                                    </div> : null }
                                    { self.state.showScanner === "ready" ? <div className="col-xs-12 text-center text-italic text-small">Please enable camera access in the pop-up window after pressing the scan button</div> : null }
                                </div>
                            </div> : null }

                            { self.state.error ? <Form>
                                <div className="row">
                                    <div className="col-xs-12 text-center">
                                        Your device does not allow camera access
                                    </div>
                                    <div className="col-xs-12 text-center text-italic text-small">Please enter your QR code manually included dashes</div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12">
                                        <Input className="full-width" placeholder="QR code*" name="qr_code_manually" value={this.state.qr_code_manually} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                    </div>
                                </div>
                            </Form> : null }

                            { self.state.showScanner === "scan" && !self.state.error ? <div className="row">
                                <div className="col-xs-12">
                                    <button className="full-width" onClick={self.changeCamera}>
                                        <span>Change camera</span>
                                    </button>
                                </div>
                            </div>: null }

                            { (self.state.showScanner === "ready" || self.state.showScanner === "scan") ? <div className="row">
                                <div className="col-xs-12">
                                    { self.state.showScanner === "ready" ? <button className="full-width" onClick={self.scan}>
                                        <span>Scan</span>
                                    </button> : null }
                                    { self.state.showScanner === "scan" && !self.state.error ? <button className="full-width" onClick={self.stopScan}>
                                        <span>Stop scan</span>
                                    </button> : null }
                                    { self.state.error ? <button className="full-width" onClick={self.addManually}>
                                        <span>Add QR code manually</span>
                                    </button> : null }
                                </div>
                            </div> : null }

                            { self.state.path === "add-location/step1" ? <div className="row">
                                <div className="col-xs-12">
                                    <button className="full-width" onClick={self.back}>
                                        <span>Back</span>
                                    </button>
                                </div>
                            </div>: null }

                        </div>
                    </ReactIScroll>


                </div>
            </div>
        );
    }
}