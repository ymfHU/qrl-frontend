/**
 * Copyright ©2019 Mavik Technologies LLC., Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Registration step 1 component
 * ---------------
 * - user login functions
 * - auto login
 */

import React from "react";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";

import PageHandlerStore from "../../stores/PageHandler";
import ValidationService from "../services/Validation";

import IndicatorStore from "../../stores/Indicator";
import RegistrationDataStore from "../../stores/RegistrationData";
import LoaderService from "../services/Loader";
import {browserHistory} from "react-router";
import ReactIScroll from "react-iscroll";
import iScroll from "iscroll";

export default  class RegStep1 extends React.Component {
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = RegistrationDataStore.getDataAll();

        this.next = this.next.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        IndicatorStore.setVariable("indicator", [
            {
                label: "Personal data",
                count: 1,
                status: "active"
            },
            {
                label: "QR Code",
                count: 2,
                status: "disabled"
            },
            {
                label: "Location",
                count: 3,
                status: "disabled"
            }
        ]);

        RegistrationDataStore.on("change", () => {
            this.setState(RegistrationDataStore.getDataAll());
        });

        PageHandlerStore.setVariable("title","Registration");
        PageHandlerStore.setVariable("subtitle","Personal Data");
    }

    onChange(e) {
        RegistrationDataStore.setData(e.target.name, e.target.value);
    }

    regOk(result) {
        if (result.hasOwnProperty("status") && result.status === "success" && result.hasOwnProperty("data")) {
            RegistrationDataStore.setData("role", result.data.role);
            RegistrationDataStore.setData("id", result.data.id);
            browserHistory.push("/registration/step2");
        }
    }

    regErrorMock() {
        browserHistory.push("/registration/step2"); //TODO: remove this for production
    }

    next(e){
        e.preventDefault();
        let data = {
            firstname: this.state.firstname,
            middlename: this.state.middlename,
            lastname: this.state.lastname,
            email: this.state.email,
            password: this.state.password
        };
        LoaderService.postData("guest/customer-registration", data, this.regOk, this.regErrorMock);
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form>
                            <div className="row">
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="firstname*" name="firstname" value={self.state.firstname} validations={[ValidationService.required]} onChange={self.onChange.bind(self)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="middlename" name="middlename"value={self.state.middlename} />
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="lastname*" name="lastname" value={self.state.lastname} validations={[ValidationService.required]} onChange={self.onChange.bind(self)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="email*" name="email" value={self.state.email} validations={[ValidationService.required, ValidationService.email]} onChange={self.onChange.bind(self)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" type="password" placeholder="password*" name="password" value={self.state.password} validations={[ValidationService.required]} onChange={self.onChange.bind(self)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" type="password" placeholder="repeat password*" name="password_confirm" value={self.state.password_confirm} validations={[ValidationService.required, ValidationService.passwordConfirm]} onChange={self.onChange.bind(self)}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6">
                                    <button type="button" className="full-width" onClick={() => {browserHistory.push("/");}}>
                                        <span>Back</span>
                                    </button>
                                </div>
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={self.next}>
                                        <span>Next</span>
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}