import React from "react";

import Form from "react-validation/build/form";
import Button from "react-validation/build/button";

import icon from "../../images/nice_job.png";

import PageHandlerStore from "../../stores/PageHandler";
import IndicatorStore from "../../stores/Indicator";
import {browserHistory} from "react-router";
import ReactIScroll from "react-iscroll";
import iScroll from "iscroll";

export default  class RegFinish extends React.Component {

    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.next = this.next.bind(this);
    }

    componentDidMount() {
        IndicatorStore.setVariable("indicator", []);
        PageHandlerStore.setVariable("title","Complete");
        PageHandlerStore.setVariable("subtitle","Your QRlocation is ready for work");
    }

    next(e){
        e.preventDefault();
        // PageHandlerStore.setVariable("goToPage","/my-locations");
        browserHistory.push("/my-locations");
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form>
                            <div className="row">
                                <div className="col-xs-12 text-center"><img className="nice_job" src={icon} /> </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 text-center">
                                    <Button onClick={this.next}>
                                        <span>Go to My locations</span>
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}