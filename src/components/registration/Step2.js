import React from "react";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";

import PageHandlerStore from "../../stores/PageHandler";
import ValidationService from "../services/Validation";

import IndicatorStore from "../../stores/Indicator";
import UserDataStore from "../../stores/UserData";
import RegistrationDataStore from "../../stores/RegistrationData";

import LoaderService from "../services/Loader";
import {browserHistory} from "react-router";
import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";


export default  class RegStep2 extends React.Component {
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = RegistrationDataStore.getDataAll();

        this.next = this.next.bind(this);
        this.onChange = this.onChange.bind(this);
        this.validationError = this.validationError.bind(this);
        this.loginSuccess = this.loginSuccess.bind(this);
        this.validationOk = this.validationOk.bind(this);
    }

    componentDidMount() {
        IndicatorStore.setVariable("indicator", [
            {
                label: "Personal data",
                count: 1,
                status: "finished"
            },
            {
                label: "QR Code",
                count: 2,
                status: "disabled"
            },
            {
                label: "Location",
                count: 3,
                status: "disabled"
            }
        ]);

        PageHandlerStore.setVariable("subtitle","Verify your email");
    }

    onChange(e) {
        RegistrationDataStore.setData(e.target.name, e.target.value);
    }

    loginSuccess(result){
        if (result!== undefined && result.hasOwnProperty("status") && result.status === "success" && result.hasOwnProperty("data")) {
            UserDataStore.setData("id", result.data.id);
            UserDataStore.setData("role", result.data.role);
            UserDataStore.setData("username", result.data.username);
            UserDataStore.setData("token", result.data.token);
            UserDataStore.setData("password", this.state.password);
            // PageHandlerStore.setVariable("goToPage","/registration/step3");
            browserHistory.push("/registration/step3");
        }
    }

    validationOk() {
        let data = { email: this.state.email, password: this.state.password};
        LoaderService.postData("guest/login", data, this.loginSuccess);
    }

    validationError() { //TODO: remove for production
        //if (result!== undefined && result.hasOwnProperty("status") && result.status === "error" && result.hasOwnProperty("message") && result.message === "This account has already validated.") {
        //    let data = { email: this.state.email, password: this.state.password};
        //    LoaderService.postData("guest/login", data, this.loginSuccess);
        //}
    }

    next(e){
        e.preventDefault();
        let data = {
            email: this.state.email,
            pincode: this.state.email_verification_code,
        };
        LoaderService.postData("guest/email-verification", data, this.validationOk, this.validationError);
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form>
                            <div className="row">
                                <div className="col-xs-12 text-center">Please check yout email and enter the verification code below</div>
                                <div className="col-xs-12 text-center text-italic text-small">If the verification email does not arrived please click the back button and check your email address</div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="verification code*" name="email_verification_code" value={this.state.email_verification_code} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6">
                                    <button type="button" className="full-width" onClick={() => {browserHistory.push("/registration/step1");}}>
                                        <span>Back</span>
                                    </button>
                                </div>
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={this.next}>
                                        <span>Next</span>
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}