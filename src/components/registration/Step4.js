import React from "react";

import Form from "react-validation/build/form";
import Button from "react-validation/build/button";
import Textarea from "react-validation/build/textarea";

import PageHandlerStore from "../../stores/PageHandler";

import IndicatorStore from "../../stores/Indicator";
import QRDataStore from "../../stores/QRdata";
import Input from "react-validation/build/input";
import ValidationService from "../services/Validation";
import Loader from "../services/Loader";
import UserDataStore from "../../stores/UserData";
import {browserHistory} from "react-router";
import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";

export default  class RegStep4 extends React.Component {
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = {
            lockcode: "",
            lock_description: "",
            path: window.location.pathname.replace("/","")
        };

        this.back = this.back.bind(this);
        this.next = this.next.bind(this);
        this.onChange = this.onChange.bind(this);
        this.lockCodeSuccess = this.lockCodeSuccess.bind(this);
        this.lockCodeError = this.lockCodeError.bind(this);
    }

    componentDidMount() {
        if (UserDataStore.getData("role") !== "customer") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("/");
            return;
        }
        switch ( this.state.path ) {
        case "add-location/step2":
            IndicatorStore.setVariable("indicator", [
                {
                    label: "QR Code",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "Location",
                    count: 2,
                    status: "disabled"
                }
            ]);
            PageHandlerStore.setVariable("title","Add Location");
            PageHandlerStore.setVariable("subtitle","Review QRLocation data");
            break;
        default:
            IndicatorStore.setVariable("indicator", [
                {
                    label: "Personal data",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "QR Code",
                    count: 2,
                    status: "finished"
                },
                {
                    label: "Location",
                    count: 3,
                    status: "disabled"
                }
            ]);
            PageHandlerStore.setVariable("title","Registration");
            PageHandlerStore.setVariable("subtitle","Review QRLocation data");
            break;
        }

        this.setState(QRDataStore.getDataAll());

        QRDataStore.on("change", () => {
            this.setState(QRDataStore.getDataAll());
        });

    }

    onChange(e) {
        QRDataStore.setData(e.target.name, e.target.value);
    }

    lockCodeSuccess() {
        if (this.state.path === "add-location/step2") {
            // PageHandlerStore.setVariable("goToPage","/add-location/step3");
            browserHistory.push("/add-location/step3");
        }
        else {
            // PageHandlerStore.setVariable("goToPage","/registration/step5");
            browserHistory.push("/registration/step5");
        }
    }

    lockCodeError() {
        //PageHandlerStore.setVariable("goToPage","/registration/step5"); //TODO: remove for production
    }

    back(e){
        e.preventDefault();
        if (this.state.path === "add-location/step2") {
            // PageHandlerStore.setVariable("goToPage","/add-location/step1");
            browserHistory.push("/add-location/step1");
        }
        else {
            // PageHandlerStore.setVariable("goToPage","/registration/step3");
            browserHistory.push("/add-location/step3");
        }
    }

    next(e){
        e.preventDefault();
        let data = {qrcode_id: QRDataStore.getData("id"), pincode: QRDataStore.getData("lockcode"), description: QRDataStore.getData("lock_description")};
        Loader.postData("customer/lockcode", data, this.lockCodeSuccess, this.lockCodeError);
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form>
                            <div className="row">
                                <div className="col-xs-12 text-center">Please review your QRlocation data below and set the lock code and the description!</div>
                                <div className="col-xs-12 text-center text-italic text-small">If the data does not match, <br />please click the back button and <br />scan the QR Code again!</div>
                                <div className="col-xs-4">
                                    Type:
                                </div>
                                <div className="col-xs-8">
                                    <div className="databox left">{this.state.boxtype_name}</div>
                                </div>
                                <div className="col-xs-4">
                                    Color:
                                </div>
                                <div className="col-xs-8">
                                    <div className="databox left">{this.state.colorcode_name}</div>
                                </div>
                                <div className="col-xs-4">
                                    Keypad:
                                </div>
                                <div className="col-xs-8">
                                    <div className="databox left">{this.state.keypadtype_name}</div>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="lockcode*" name="lockcode" value={this.state.lockcode} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Textarea className="full-width" placeholder="description*" name="lock_description" value={this.state.lock_description} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6">
                                    <button type="button" className="full-width" onClick={this.back}>
                                        <span>Back</span>
                                    </button>
                                </div>
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={this.next}>
                                        <span>Next</span>
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}