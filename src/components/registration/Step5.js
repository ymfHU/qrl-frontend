import React from "react";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";

import PageHandlerStore from "../../stores/PageHandler";
import ValidationService from "../services/Validation";

import IndicatorStore from "../../stores/Indicator";
import RegistrationDataStore from "../../stores/RegistrationData";
import QRDataStore from "../../stores/QRdata";
import UserDataStore from "../../stores/UserData";
import Autocomplete from "react-google-autocomplete";
import {browserHistory} from "react-router";
import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";
// import loadGoogleMapsApi from "load-google-maps-api";

export default  class RegStep5 extends React.Component {
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = {
            cities: [],
            formdata: RegistrationDataStore.getDataAll(),
            path: window.location.pathname.replace("/","")
        };

        this.next = this.next.bind(this);
        this.back = this.back.bind(this);
    }

    componentDidMount() {
        if (UserDataStore.getData("token") === "") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("/");
            return;
        }

        switch ( this.state.path ) {
        case "add-location/step3":
            IndicatorStore.setVariable("indicator", [
                {
                    label: "QR Code",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "Location",
                    count: 2,
                    status: "active"
                }
            ]);
            PageHandlerStore.setVariable("title","Add Location");
            PageHandlerStore.setVariable("subtitle","Set your location");
            break;
        case "edit":
            IndicatorStore.setVariable("indicator", [
                {
                    label: "Edit Location",
                    count: 1,
                    status: "active"
                },
                {
                    label: "Review Location",
                    count: 2,
                    status: "disabled"
                }
            ]);

            RegistrationDataStore.setData("address", QRDataStore.getData("address1"));
            RegistrationDataStore.setData("state", QRDataStore.getData("state"));
            RegistrationDataStore.setData("city", QRDataStore.getData("city"));
            RegistrationDataStore.setData("zip", QRDataStore.getData("zip"));
            PageHandlerStore.setVariable("title","Edit location");
            PageHandlerStore.setVariable("subtitle","Edit location details");
            break;
        default:
            IndicatorStore.setVariable("indicator", [
                {
                    label: "Personal data",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "QR Code",
                    count: 2,
                    status: "finished"
                },
                {
                    label: "Location",
                    count: 3,
                    status: "active"
                }
            ]);

            PageHandlerStore.setVariable("title","Registration");
            PageHandlerStore.setVariable("subtitle","Set your location");
            break;
        }

        RegistrationDataStore.on("change", () => {
            this.setState({formdata: RegistrationDataStore.getDataAll()});
        });
    }

    onChange(e) {
        RegistrationDataStore.setData(e.target.name, e.target.value);
    }

    next(e){
        e.preventDefault();
        switch ( this.state.path ) {
        case "edit":
            QRDataStore.setData("address1", RegistrationDataStore.getData("address"));
            QRDataStore.setData("state", RegistrationDataStore.getData("state"));
            QRDataStore.setData("city", RegistrationDataStore.getData("city"));
            QRDataStore.setData("zip", RegistrationDataStore.getData("zip"));
            // PageHandlerStore.setVariable("goToPage","/review");
            browserHistory.push("/review");
            break;
        case "add-location/step3":
            // PageHandlerStore.setVariable("goToPage","/add-location/step4");
            browserHistory.push("/add-location/step4");
            break;
        default:
            // PageHandlerStore.setVariable("goToPage","/registration/step6");
            browserHistory.push("/registration/step6");
            break;
        }
    }

    back(e){
        e.preventDefault();
        switch ( this.state.path ) {
        case "add-location/step3":
            // PageHandlerStore.setVariable("goToPage","/add-location/step2");
            browserHistory.push("/add-location/step2");
            break;
        case "edit":
            // PageHandlerStore.setVariable("goToPage","/my-locations");
            browserHistory.push("/my-locations");
            break;
        default:
            // PageHandlerStore.setVariable("goToPage","/registration/step4");
            browserHistory.push("/registration/step4");
            break;
        }
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form>
                            <div className="row">
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="address*" name="address" value={this.state.formdata.address} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Autocomplete
                                        style={{width: "100%"}}
                                        onPlaceSelected={(place) => {
                                            RegistrationDataStore.setData("city", place.address_components[0].long_name);
                                            RegistrationDataStore.setData("state",place.address_components[2].long_name);
                                        }}
                                        types={["(regions)"]}
                                        placeholder={"Choose your city, town"}
                                        // componentRestrictions={{country: "us"}}
                                        defaultValue={this.state.formdata.city}
                                        validations={[ValidationService.required]}
                                    />
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="Choose your state, provence*" name="state" value={this.state.formdata.state} validations={[ValidationService.required]}/>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="zip code*" name="zip"value={this.state.formdata.zip} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={this.back}>
                                        <span>Back</span>
                                    </Button>
                                </div>
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={this.next}>
                                        <span>Next</span>
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}