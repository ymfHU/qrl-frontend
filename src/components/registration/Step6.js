import React from "react";
// import PropTypes from "prop-types";
import Geocode from "react-geocode";
import GoogleMapReact from "google-map-react";


import marker from "../../images/marker.png";

import Form from "react-validation/build/form";
import Button from "react-validation/build/button";

import PageHandlerStore from "../../stores/PageHandler";

import IndicatorStore from "../../stores/Indicator";
import QRDataStore from "../../stores/QRdata";
import RegistrationDataStore from "../../stores/RegistrationData";
// import DeviceInfoStore from "../../stores/DeviceInfoStore";
import Loader from "../services/Loader";
import UserDataStore from "../../stores/UserData";
import {browserHistory} from "react-router";
import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";

class LocationIcon extends React.Component {
    render() {
        return <img src={marker} className="map_marker" />;
    }
}

// LocationIcon.defaultProps = {
//     text: "hello"
// };
//
// LocationIcon.propTypes = {
//     text: PropTypes.string.isRequired,
// };

export default  class RegStep6 extends React.Component {

    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        // set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
        Geocode.setApiKey("AIzaSyCuv4pnWSAuoNZLUQZnd6REItQVmLJmEpg");

        // Enable or disable logs. Its optional.
        // Geocode.enableDebug();

        this.state = {
            zoom: 19,
            path: window.location.pathname.replace("/","")
        };

        this.getLatLng = this.getLatLng.bind(this);
        this.next = this.next.bind(this);
        this.onChange = this.onChange.bind(this);
        this.back = this.back.bind(this);
        this.qrPutSuccess = this.qrPutSuccess.bind(this);
    }

    componentDidMount() {
        if (UserDataStore.getData("token") === "") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("/");
            return;
        }

        switch ( this.state.path ) {
        case "add-location/step4":
            IndicatorStore.setVariable("indicator", [
                {
                    label: "QR Code",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "Location",
                    count: 2,
                    status: "finished"
                }
            ]);
            PageHandlerStore.setVariable("title","Add Location");
            PageHandlerStore.setVariable("subtitle","Review Location");
            break;
        case "review":
            IndicatorStore.setVariable("indicator", [
                {
                    label: "Edit Location",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "Review Location",
                    count: 2,
                    status: "active"
                }
            ]);

            RegistrationDataStore.setData("address", QRDataStore.getData("address1"));
            RegistrationDataStore.setData("state", QRDataStore.getData("state"));
            RegistrationDataStore.setData("city", QRDataStore.getData("city"));
            RegistrationDataStore.setData("zip", QRDataStore.getData("zip"));
            PageHandlerStore.setVariable("title","Review location");
            PageHandlerStore.setVariable("subtitle","Review your location");
            break;
        default:
            IndicatorStore.setVariable("indicator", [
                {
                    label: "Personal data",
                    count: 1,
                    status: "finished"
                },
                {
                    label: "QR Code",
                    count: 2,
                    status: "finished"
                },
                {
                    label: "Location",
                    count: 3,
                    status: "finished"
                }
            ]);

            PageHandlerStore.setVariable("title","Registration");
            PageHandlerStore.setVariable("subtitle","Review your location");
            break;
        }

        this.setState(QRDataStore.getDataAll());

        QRDataStore.on("change", () => {
            this.setState(QRDataStore.getDataAll());
        });

        this.getLatLng();
    }

    getLatLng() {
        // Get latidude & longitude from address.
        // Geocode.fromAddress("2817 Newbern Way Clearwater Florida 33761").then(
        let address = RegistrationDataStore.getData("address") + " " + RegistrationDataStore.getData("city") + "," + RegistrationDataStore.getData("state") + " " + RegistrationDataStore.getData("zip");

        Geocode.fromAddress(address).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;
                this.setState({
                    center:
                        {
                            lat: lat,
                            lng: lng
                        }
                });
            },
            error => {
                console.error(error);
            }
        );
    }

    onChange(e) {
        QRDataStore.setData(e.target.name, e.target.value);
    }

    qrPutSuccess() {
        switch ( this.state.path ) {
        case "add-location/step4":
            // PageHandlerStore.setVariable("goToPage","/add-location/finish");
            browserHistory.push("/add-location/finish");
            break;
        case "review":
            // PageHandlerStore.setVariable("goToPage","/my-locations");
            browserHistory.push("/my-locations");
            break;
        default:
            // PageHandlerStore.setVariable("goToPage","/registration/finish");
            browserHistory.push("/registration/finish");
            break;
        }
    }

    next(e){
        e.preventDefault();
        let reg_data = RegistrationDataStore.getDataAll(),
            data = {
                //description: "string",
                address1: reg_data.address,
                //address2: "string",
                state: reg_data.state,
                zip: reg_data.zip,
                city: reg_data.city,
                latitude: "" + this.state.center.lat,
                longitude: "" + this.state.center.lng
            };
        Loader.putData("customer/qr-code/" + QRDataStore.getData("qrcode"), data, this.qrPutSuccess);
    }

    back(e){
        e.preventDefault();
        switch ( this.state.path ) {
        case "add-location/step4":
            // PageHandlerStore.setVariable("goToPage","/add-location/step3");
            browserHistory.push("/add-location/step3");
            break;
        case "review":
            // PageHandlerStore.setVariable("goToPage","/edit");
            browserHistory.push("/edit");
            break;
        default:
            // PageHandlerStore.setVariable("goToPage","/registration/step5");
            browserHistory.push("/registration/step5");
            break;
        }
    }

    getGoogleMapsOptions(){
        return {
            streetViewControl: false,
            scaleControl: true,
            fullscreenControl: false,
            styles: [{
                featureType: "poi.business",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            }],
            gestureHandling: "greedy",
            disableDoubleClickZoom: true,
            // minZoom: 11,
            // maxZoom: 11,

            mapTypeControl: true,
            mapTypeId: "satellite",
            tilt: 0,
            disableDefaultUI: true,
            mapTypeControlOptions: {
                // style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: this.state.center,
                // mapTypeIds: [
                //     maps.MapTypeId.ROADMAP,
                //     maps.MapTypeId.SATELLITE,
                //     maps.MapTypeId.HYBRID
                // ]
            },
            zoomControl: true,
            clickableIcons: false
        };
    }

    getAddressCityState() {
        return RegistrationDataStore.getData("city") + ", " + RegistrationDataStore.getData("state") + " " + RegistrationDataStore.getData("zip");
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form>
                            <div className="row">
                                <div className="col-xs-12 text-center">Please review your location!</div>
                                <div className="col-xs-12 text-center text-italic text-small">If the data does not match, please click the back button and correct your address!</div>
                                <div className="col-xs-12">
                                    <div style={{ height: "250px", width: "100%" }}>
                                        { this.state.center != undefined ? <GoogleMapReact
                                            bootstrapURLKeys={{ key: "AIzaSyCuv4pnWSAuoNZLUQZnd6REItQVmLJmEpg" }}
                                            defaultCenter={this.state.center}
                                            defaultZoom={this.state.zoom}
                                            options={this.getGoogleMapsOptions()}
                                        >
                                            <LocationIcon
                                                lat={this.state.center.lat}
                                                lng={this.state.center.lng}
                                                // text="x"
                                            />
                                        </GoogleMapReact> : null }
                                    </div>
                                </div>
                                <div className="col-xs-12">
                                    <div className="databox big text-center">
                                        <div className="text-center">
                                            {RegistrationDataStore.getData("address")}
                                        </div>
                                        <div className="text-center">
                                            {this.getAddressCityState()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={this.back}>
                                        <span>Back</span>
                                    </Button>
                                </div>
                                <div className="col-xs-6">
                                    <Button className="full-width" onClick={this.next}>
                                        <span>Finish</span>
                                    </Button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}