import React from "react";

const Error = () => {
    return (
        <div>
            Error: the page does not exists!
        </div>
    );
};

export default Error;