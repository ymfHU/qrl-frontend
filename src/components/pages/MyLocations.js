import React from "react";

import PageHandlerStore from "../../stores/PageHandler";

import IndicatorStore from "../../stores/Indicator";
import LocationsDataStore from "../../stores/LocationsData";
import LoaderService from "../services/Loader";
import UserDataStore from "../../stores/UserData";
import QRDataStore from "../../stores/QRdata";
import {browserHistory} from "react-router";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus, faSearchLocation, faPen, faMedkit} from "@fortawesome/free-solid-svg-icons";
import Form from "react-validation/build/form";
import ValidationService from "../services/Validation";
import Textarea from "react-validation/build/textarea";
import Button from "react-validation/build/button";
import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";

export default  class MyLocations extends React.Component {
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = {
            addEmergencyInfo: false,
            emergency_info: "",
            data: []
        };

        this.showDetails = this.showDetails.bind(this);
        this.edit = this.edit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        let self = this;

        if (UserDataStore.getData("token") === "") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("");
            return;
        }

        LocationsDataStore.on("change", () => {
            self.setState(LocationsDataStore.getDataAll());
        });

        IndicatorStore.setVariable("indicator", []);
        PageHandlerStore.setVariable("title","My locations");
        PageHandlerStore.setVariable("subtitle","");
        LoaderService.getData("customer/get-all-qr-code", "", this.getLocationsSuccess, this.getLocationsError);
    }

    showDetails(item) {
        QRDataStore.setData(item);
        // PageHandlerStore.setVariable("goToPage","/details");
        browserHistory.push("/details");
    }

    edit(item) {
        QRDataStore.setData(item);
        // PageHandlerStore.setVariable("goToPage","/edit");
        browserHistory.push("/edit");
    }

    editEmergency(item) {
        if(item.emergency_info === null) { item.emergency_info = ""; }
        this.setState({addEmergencyInfo: !this.state.addEmergencyInfo, emergency_info: item.emergency_info});
    }

    getLocationsSuccess(result) {
        LocationsDataStore.setData("data", result);
    }

    getLocationsError() {
        //PageHandlerStore.setVariable("goToPage","/registration/step2"); //TODO: remove this for production
        // browserHistory.push("/registration/step2");
    }

    getAddressCityState(item) {
        // address1: "2817 Newbern Way"
        // address2: null
        // boxtype_name: "Demo Box Type Name"
        // city: "Clearwater"
        // colorcode_name: "Demo Color Name"
        // description: "Demo Qr Code Description"
        // id: 1
        // keypadtype_name: "Demo Keypad Type Name"
        // keypadtype_version: "1.0"
        // latitude: "37.5756039"
        // longitude: "-122.3432705"
        // qrcode: "JM5J-JE8T-F312-FO4W"
        // state: "Florida"
        // zip: 33761

        return item.city + " " + item.state + " " + item.zip;
    }

    hasEmergencyInfo(item) {
        return item.emergency_info === null || item.emergency_info ==="" ? "blink" : "";
    }

    onChange(e) {
        let self = this;
        if (e.target.name === "emergency_info") {
            self.setState({emergency_info: e.target.value});
        }
    }

    render()
    {
        let self = this;
        return (
            <div className="content">
                <div className="wrapper my-locations-list">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <button className="full-width" onClick={() => {browserHistory.push("/add-location/step1");}}>
                                        <span><FontAwesomeIcon icon={faPlus} /> Add new location</span>
                                    </button>
                                </div>
                            </div>
                            <div className="row decorator">
                                <div className="col-xs-12">
                                    <div className="decorator-line" />
                                </div>
                            </div>
                            {self.state.data.map((item, i) => {
                                return (
                                    <React.Fragment  key={i}>
                                        <div className="row">
                                            <div className="col-xs-12">
                                                <div className="text-center">
                                                    <div className="text-center highlighted">
                                                        {item.address1}
                                                    </div>
                                                    <div className="text-center">
                                                        {this.getAddressCityState(item)}
                                                    </div>
                                                    <div className="text-center">
                                                        <button className="icon" onClick={() => { this.showDetails(item); }}>
                                                            <FontAwesomeIcon icon={faSearchLocation} />
                                                            <div className="btn-title">
                                                                Show Details
                                                            </div>
                                                        </button>
                                                        <button className="icon" onClick={() => { this.edit(item); }}>
                                                            <FontAwesomeIcon icon={faPen} />
                                                            <div className="btn-title">
                                                                Edit Location
                                                            </div>
                                                        </button>
                                                        <button className={"icon " + this.hasEmergencyInfo(item)} onClick={() => { this.editEmergency(item); }}>
                                                            <FontAwesomeIcon icon={faMedkit} />
                                                            <div className="btn-title">
                                                                Emergency Info
                                                            </div>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className={"add-code-holder " + this.state.addEmergencyInfo}>
                                            <div className="row">
                                                <div className="col-xs-12 text-center subtitle highlighted">Add Emergency Information:</div>
                                            </div>
                                            <Form>
                                                <div className="row">
                                                    <div className="col-xs-12">
                                                        <Textarea className="full-width emergencyinfo-input" placeholder="Emergency information*" name="emergency_info" value={this.state.emergency_info} validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-xs-12">
                                                        <Button className="full-width" onClick={this.addEmergencyInformation}>
                                                            <span>Change Emergency Information</span>
                                                        </Button>
                                                    </div>
                                                </div>
                                            </Form>
                                        </div>
                                        { item.emergency_info === null || item.emergency_info ==="" ?
                                            <div className="row notification-cell">
                                                <div className="col-xs-12 text-center">
                                                    <FontAwesomeIcon icon={faMedkit} /> No Emergency Information set to this address.
                                                </div>
                                            </div> : null }
                                        <div className="row decorator">
                                            <div className="col-xs-12">
                                                <div className="decorator-line" />
                                            </div>
                                        </div>
                                    </React.Fragment>
                                );
                            })}
                        </div>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}