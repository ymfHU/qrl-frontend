import React from "react";
// import PropTypes from "prop-types";
// import GoogleMapReact from "google-map-react";

import PageHandlerStore from "../../stores/PageHandler";
import cloneDeep from "lodash/cloneDeep";
import IndicatorStore from "../../stores/Indicator";
import QRDataStore from "../../stores/QRdata";
import UserDataStore from "../../stores/UserData";
import LoaderService from "../services/Loader";
import {browserHistory} from "react-router";

import Map from "../map/Map";
import Draggable from "../Draggable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkedAlt, faMap, faCheck, faRedoAlt, faHelicopter } from "@fortawesome/free-solid-svg-icons";
import LockCodes from "../LockCodes";

export default  class Details extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isDarkMode: PageHandlerStore.getValue("isDarkMode"),
            addNewCode: false,
            edit_locations: false,
            edit_dropzone: false,
            mapType: "SATELLITE",
            qr_data: QRDataStore.getDataAll(),
            role: UserDataStore.getData("role")
        };
        this.next = this.next.bind(this);
        this.packageDelivered = this.packageDelivered.bind(this);
        this.packageDeliveredSuccess = this.packageDeliveredSuccess.bind(this);
        this.packageDeliveredError = this.packageDeliveredError.bind(this);
        this.editPositions = this.editPositions.bind(this);
        this.setLockboxCoordinates = this.setLockboxCoordinates.bind(this);
        this.setDropzoneCoordinates = this.setDropzoneCoordinates.bind(this);
        this.getLockboxCoordinates = this.getLockboxCoordinates.bind(this);
        this.geDropzoneCoordinates = this.geDropzoneCoordinates.bind(this);
    }

    componentDidMount() {
        if (UserDataStore.getData("token") === "") {
            // PageHandlerStore.setVariable("goToPage","/");
            browserHistory.push("/");
            return;
        }

        UserDataStore.on("change", () => {
            this.setState({rolle: UserDataStore.getData("role")});
        });

        this.setState({
            center:
                {lat: Number(this.state.qr_data.latitude), lng: Number(this.state.qr_data.longitude)},
            zoom: 19,
            lockboxes: [{lat: Number(this.state.qr_data.latitude), lng: Number(this.state.qr_data.longitude)}]
        });

        IndicatorStore.setVariable("indicator", []);

        QRDataStore.on("change", () => {
            this.setState({qr_data: QRDataStore.getDataAll()});
        });

        PageHandlerStore.setVariable("title","Location details");
        PageHandlerStore.setVariable("subtitle","");

        PageHandlerStore.on("change", () => {
            this.setState({
                isDarkMode: PageHandlerStore.getValue("isDarkMode"),
            });
        });
    }

    getBgColor() {
        return window.qrLocation.colors[this.state.isDarkMode]["--color-background"];
    }

    next(e){
        e.preventDefault();
    }

    getAddressCityState() {
        return this.state.qr_data.city + ", " + this.state.qr_data.state + " " + this.state.qr_data.zip;
    }

    editPositions() {
        this.setState({edit_locations: !this.state.edit_locations});
    }

    editDropzone() {
        this.setState({edit_dropzone: !this.state.edit_dropzone});
    }

    changeMapType() {
        switch (this.state.mapType ) {
        case "HYBRID":
            this.setState({mapType: "ROADMAP"});
            break;
        case "ROADMAP":
            this.setState({mapType: "SATELLITE"});
            break;
        case "SATELLITE":
            this.setState({mapType: "HYBRID"});
            break;
        default:
            this.setState({mapType: "SATELLITE"});
            break;
        }
    }

    packageDelivered(){
        LoaderService.getData("courier/delivery", this.state.qr_data.qrcode, this.packageDeliveredSuccess, this.packageDeliveredError);
    }

    packageDeliveredSuccess() {
        // PageHandlerStore.setVariable("goToPage","/scan");
        browserHistory.push("/scan");
    }

    packageDeliveredError() {
        //TODO: error handling if needed
    }

    setLockboxCoordinatesSuccess(res) {
        console.log(res);
    }

    setLockboxCoordinates(coordinates) {
        let qr_data = cloneDeep(this.state.qr_data);

        if (qr_data.hasOwnProperty("lock_codes")) {
            delete qr_data.lock_codes;
        }

        if (qr_data.drone_dropzone === null) {
            QRDataStore.setData("drone_dropzone",  JSON.stringify({lockbox: coordinates.lockbox, dropzone: {}}));
            qr_data.drone_dropzone = {lockbox: coordinates.lockbox, dropzone: {}};
        }
        else {
            qr_data.drone_dropzone = JSON.parse(qr_data.drone_dropzone);
            QRDataStore.setData("drone_dropzone", JSON.stringify({lockbox: coordinates.lockbox, dropzone: qr_data.drone_dropzone.dropzone}));
            qr_data.drone_dropzone.lockbox = coordinates.lockbox;
        }
        qr_data.drone_dropzone = JSON.stringify(qr_data.drone_dropzone);
        LoaderService.putData("customer/qr-code/" + QRDataStore.getData("qrcode"), qr_data, this.setLockboxCoordinatesSuccess);
    }

    setDropzoneCoordinates(coordinates) {
        let qr_data = cloneDeep(this.state.qr_data);

        if (qr_data.hasOwnProperty("lock_codes")) {
            delete qr_data.lock_codes;
        }

        if (qr_data.drone_dropzone === null) {
            QRDataStore.setData("drone_dropzone", JSON.stringify({lockbox: {}, dropzone: coordinates.dropzone}));
            qr_data.drone_dropzone = {lockbox: {}, dropzone: coordinates.dropzone};
        }
        else {
            qr_data.drone_dropzone = JSON.parse(qr_data.drone_dropzone);
            QRDataStore.setData("drone_dropzone", JSON.stringify({lockbox: qr_data.drone_dropzone.lockbox, dropzone: coordinates.dropzone}));
            qr_data.drone_dropzone.dropzone = coordinates.dropzone;
        }
        qr_data.drone_dropzone = JSON.stringify(qr_data.drone_dropzone);
        LoaderService.putData("customer/qr-code/" + QRDataStore.getData("qrcode"), qr_data, this.setLockboxCoordinatesSuccess);
    }

    getLockboxCoordinates() {
        let _json = this.state.qr_data.drone_dropzone;
        console.log(_json);
        if (_json !== null && _json !=="" && _json !== undefined) {
            let data = JSON.parse(_json);
            console.log(data);
            if (data.hasOwnProperty("lockbox")) {
                return data.lockbox;
            }
        }
    }

    geDropzoneCoordinates() {
        let _json = this.state.qr_data.drone_dropzone;
        console.log(_json);
        if (_json !== null && _json !=="" && _json !== undefined) {
            let data = JSON.parse(_json);
            console.log(data);
            if (data.hasOwnProperty("dropzone")) {
                return data.dropzone;
            }
        }
    }

    render()
    {
        let self = this;
        console.log(this.state.qr_data);
        return (
            <div className="content">
                <div className="wrapper">
                    <Map
                        mapStyle={window.qrLocation.mapColors[self.state.isDarkMode]}
                        mapType={this.state.mapType}
                        editDropzone={this.state.edit_dropzone}
                        editLocations={this.state.edit_locations}
                        setLockboxCoordinates={this.setLockboxCoordinates}
                        setDropzoneCoordinates={this.setDropzoneCoordinates}
                        left={"-20px"}
                        right={"-20px"}
                        center={{
                            lat: Number(this.state.qr_data.latitude),
                            lng: Number(this.state.qr_data.longitude)
                        }}
                        lockboxes={[
                            this.getLockboxCoordinates()
                        ]}
                        langingzones={[
                            this.geDropzoneCoordinates()
                        ]}
                    />

                    <Draggable
                        left={"-20px"}
                        right={"-20px"}
                        startPos={150}
                        minPos={150}
                        splashColor={self.getBgColor()}
                        useClass={"draggable"}>
                        <div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="databox big text-center">
                                        <div className="text-center">
                                            {this.state.qr_data.address1}
                                        </div>
                                        <div className="text-center">
                                            {this.getAddressCityState()}
                                        </div>
                                        <div className="text-center">
                                            { this.state.role === "customer" ?
                                                <button className="icon" onClick={() => {this.editPositions();}}>
                                                    <FontAwesomeIcon icon={faMapMarkedAlt} />
                                                    <div className="btn-title">
                                                        Lockbox location
                                                    </div>
                                                </button> : null }
                                            { this.state.role === "customer" ?
                                                <button className="icon" onClick={() => {this.editDropzone();}}>
                                                    <FontAwesomeIcon icon={faHelicopter} />
                                                    <div className="btn-title">
                                                        Dropzone location
                                                    </div>
                                                </button> : null }
                                            <button className="icon" onClick={() => {this.changeMapType();}}>
                                                <FontAwesomeIcon icon={faMap} />
                                                <div className="btn-title">
                                                    Map Style
                                                </div>
                                            </button>
                                            { this.state.role === "courier_employee" ?
                                                <button className="icon" onClick={() => {this.packageDelivered();}}>
                                                    <FontAwesomeIcon icon={faCheck} />
                                                    <div className="btn-title">
                                                        Package Delivered
                                                    </div>
                                                </button> : null }
                                            { this.state.role === "customer" ?
                                                <button className="icon" onClick={() => {browserHistory.push("/my-locations");}}>
                                                    <FontAwesomeIcon icon={faRedoAlt} />
                                                    <div className="btn-title">
                                                        My locations
                                                    </div>
                                                </button> : null }
                                            { this.state.role === "courier_employee" ?
                                                <button className="icon" onClick={() => {browserHistory.push("/scan");}}>
                                                    <FontAwesomeIcon icon={faRedoAlt} />
                                                    <div className="btn-title">
                                                        Scan
                                                    </div>
                                                </button> : null }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row decorator">
                                <div className="col-xs-12">
                                    <div className="decorator-line" />
                                </div>
                            </div>
                            <LockCodes
                                qrCodeId={this.state.qr_data.id}
                                role={this.state.role}
                            />
                        </div>
                    </Draggable>

                    <div className="row">
                        <div className="col-xs-12">
                            { this.state.role === "customer" ? <a href="" className="button" onClick={() => {browserHistory.push("/my-locations");}}>Back to My locations</a> : null }
                            { this.state.role === "courier_employee" ? <a href="" className="button" onClick={() => {browserHistory.push("/scan");}}>Back to Scan</a> : null }
                        </div>
                        <div className="col-xs-6">
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}