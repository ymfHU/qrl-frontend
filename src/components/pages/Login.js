/**
 * Copyright ©2019 Mavik Technologies LLC., Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Login component
 * ---------------
 * - user login functions
 * - auto login
 */

/**
 * Import used react components
 */
import React from "react";
import { browserHistory } from "react-router";

/**
 * Import used stores
 */
import PageHandlerStore from "../../stores/PageHandler";
import IndicatorStore from "../../stores/Indicator";
import userDataStore from "../../stores/UserData";

/**
 * Import used services
 */
import ValidationService from "../services/Validation";
import AutologinService from "../services/AutoLogin";
import LoaderService from "../services/Loader";

/**
 * Import other used components
 */
// import validator from "validator";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import Button from "react-validation/build/button";
import ReactIScroll from "react-iscroll";
import iScroll from "iscroll";

export default class Login extends React.Component {

    /**
     * Set the component defaults
     * @param props
     */
    constructor(props) {
        super(props);

        this.getRef = "iScroll";
        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = {
            email: null,
            password: null
        };

        /**
         * Bind functions to this
         */
        this.fetchLogin = this.fetchLogin.bind(this);
        this.loginSuccess = this.loginSuccess.bind(this);

        /**
         * Start AutologinService
         */
        AutologinService.tryLogin(this.loginSuccess);
    }

    /**
     * Set things when component is mounting
     */
    componentDidMount() {
        /**
         * Set the header indicator
         */
        IndicatorStore.setVariable("indicator", []);

        /**
         * Set page title and subtitle
         */
        PageHandlerStore.setVariable("title","Login");
        PageHandlerStore.setVariable("subtitle","");

        /**
         * Set menu visibility state
         */
        PageHandlerStore.setVariable("menuActive",false);

        /**
         * Reset token
         */
        userDataStore.setData("token", "");
    }

    /**
     * Save changes to component state
     * @param e
     */
    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    /**
     * Handle login success
     * @param result
     */
    loginSuccess(result){
        if (result.hasOwnProperty("status") && result.status === "success" && result.hasOwnProperty("data")) {
            /**
             * Save user data to userDataStore
             */
            userDataStore.setData("id", result.data.id);
            userDataStore.setData("role", result.data.role);
            userDataStore.setData("username", result.data.username);
            userDataStore.setData("token", result.data.token);

            /**
             * Save data for AutologinService
             */
            if (!AutologinService.isAutologin()) {
                AutologinService.saveData({ email: this.state.email, password: this.state.password});
            }

            /**
             * Redirect user depend on role
             */
            if (result.data.role === "courier_employee") {
                browserHistory.push("/scan");
            }
            else {
                browserHistory.push("/my-locations");
            }
        }
    }

    /**
     * Fetch login data from server
     * @param e
     */
    fetchLogin(e){
        e.preventDefault();
        LoaderService.postData("guest/login", { email: this.state.email, password: this.state.password}, this.loginSuccess);
    }

    /**
     * Render dom elements
     * @returns {*}
     */
    render()
    {
        return (
            <div className="content">
                <div className="wrapper">
                    <ReactIScroll
                        ref={self.getRef}
                        iScroll={iScroll}
                        options={self.scrollOptions}
                    >
                        <Form name="loginForm">
                            <div className="row">
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="email*" name="email" validations={[ValidationService.required, ValidationService.email]} onChange={this.onChange.bind(this)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Input className="full-width" placeholder="password*" type="password" name="password" validations={[ValidationService.required]} onChange={this.onChange.bind(this)}/>
                                </div>
                                <div className="col-xs-12">
                                    <Button className="full-width"  onClick={this.fetchLogin}>
                                        <span>Login</span>
                                    </Button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <ul className="separator">
                                        <li className="line">&nbsp;</li>
                                        <li className="label"><span>or</span></li>
                                        <li className="line">&nbsp;</li>
                                    </ul>
                                </div>
                                <div className="col-xs-12">
                                    <button type="button"  className="full-width" onClick={() => {browserHistory.push("/registration/step1");}}>
                                        <span>Register</span>
                                    </button>
                                </div>
                            </div>
                        </Form>
                    </ReactIScroll>
                </div>
            </div>
        );
    }
}