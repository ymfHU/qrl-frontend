import React from "react";

import QrReader from "react-qr-scanner";

import ScannerDataStore from "../stores/ScannerData";
import QRDataStore from "../stores/QRdata";

export default  class RegStep1 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            delay: 100,
            result: "No result",
            facingMode: QRDataStore.getData("facingMode"),
            legacyMode: false
        };

        this.handleScan = this.handleScan.bind(this);
        this.handleError = this.handleError.bind(this);
    }

    componentDidMount() {
        QRDataStore.on("change", () => {
            this.setState({facingMode: QRDataStore.getData("facingMode")});
        });
    }

    handleScan(data){
        if (data !== null && data !== "") {
            ScannerDataStore.setData("qrCode", data);
        }
    }

    handleError(err){
        ScannerDataStore.setData("error", true);
        console.error(err);
    }

    render()
    {
        let self = this;
        return (
            <QrReader
                facingMode = {self.state.facingMode}
                delay={self.state.delay}
                onError={self.handleError}
                onScan={self.handleScan}
            />
        );
    }
}