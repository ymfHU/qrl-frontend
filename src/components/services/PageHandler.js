import React from "react";
import { Redirect, withRouter } from "react-router";
import PageHandlerStore from "../../stores/PageHandler";

export default withRouter(class PageHandler extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            goToPage: PageHandlerStore.getValue("goToPage")
        };

        this.resetState = this.resetState.bind(this, true);

        PageHandlerStore.on("change", () => {
            this.setState({
                goToPage: PageHandlerStore.getValue("goToPage"),
            });

            if ( this.state.goToPage !== "" ) {
                console.log("gotopage: ",this.state.goToPage);
            }
        });
    }

    resetState() {
        PageHandlerStore.setVariable("goToPage","");
    }

    render()
    {
        if ( this.state.goToPage !== "" ) {
            this.resetState();
            return <Redirect to={this.state.goToPage}/>;
        }
        return null;
    }
});