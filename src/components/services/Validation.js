// import validator from "validator";
import React from "react";
import validator from "validator";


class ValidationService  {
    required ( value) {
        if (value === null || !value.toString().trim().length) {
            // We can return string or jsx as the 'error' prop for the validated Component
            return <span className="validate-error">This field is required.</span>;
        }
    }

    email ( value ) {
        if (value === null || !validator.isEmail(value)) {
            return <span className="validate-error">Please enter valid email address</span>;
        }
    }

    emailConfirm (value, props, components) {
        if (value === null || !validator.isEmail(value)) {
            return "Please enter valid email address";
        }
        if (value === null || value !== components["email"][0].value) {
            return <span className="validate-error">Emails are not equal.</span>;
        }
    }

    lt (value, props) {
        // get the maxLength from component's props
        if (value === null || !value.toString().trim().length > props.maxLength) {
            // Return jsx
            return <span className="validate-error">The value exceeded {props.maxLength} symbols.</span>;
        }
    }

    passwordConfirm (value, props, components) {
        // NOTE: Tricky place. The 'value' argument is always current component's value.
        // So in case we're 'changing' let's say 'password' component - we'll compare it's value with 'confirm' value.
        // But if we're changing 'confirm' component - the condition will always be true
        // If we need to always compare own values - replace 'value' with components.password[0].value and make some magic with error rendering.
        if (value === null || value !== components["password"][0].value) { // components['password'][0].value !== components['confirm'][0].value
            // 'confirm' - name of input
            // components['confirm'] - array of same-name components because of checkboxes and radios
            return <span className="validate-error">Passwords are not equal.</span>;
        }
    }
}

const validationService = new ValidationService;

export default validationService;
