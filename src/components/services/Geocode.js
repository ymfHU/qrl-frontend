/**
 * Copyright ©2019 Mavik Technologies LLC., Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Google Geocode service
 * ---------------
 * - convert address to coordinates
 *
 * Example usage:
 * GeocodeService.getCoordinates("2817 Newbern Way Clearwater Florida 33761", getCoordinatesSuccess);
 */

/**
 * Import used react components
 */
import EventEmitter from "events";

/**
 * Import used vendor components
 */
import Geocode from "react-geocode";

class GeocodeService extends EventEmitter {

    /**
     * Set the component defaults
     */
    constructor() {
        super();
        // set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
        Geocode.setApiKey("AIzaSyCuv4pnWSAuoNZLUQZnd6REItQVmLJmEpg");

        // Enable or disable logs. Its optional.
        Geocode.enableDebug();
    }

    /**
     * Get coordinates from address
     * @param address
     * @param callback
     */
    getCoordinates(address, callback) {
        console.log(address);

        Geocode.fromAddress(address).then(
            response => {
                const { lat, lng } = response.results[0].geometry.location;
                callback({lat: lat, lng: lng});
            },
            error => {
                console.error(error);
            }
        );
    }
}

const geocodeService = new GeocodeService;

export default geocodeService;