import EventEmitter from "events";

class NotificationService extends EventEmitter {
    constructor() {
        super();
        this.state = {
            showNotification: false,
            title: "",
            text: "",
        };
    }

    getState() {
        return this.state;
    }

    hideNotification() {
        this.state.showNotification = false;
        this.emit("change");
    }

    showError(title, text) {
        this.state = {
            showNotification: true,
            title: title,
            text: text,
        };
        this.emit("change");
    }
}

const notificationService = new NotificationService;

export default notificationService;