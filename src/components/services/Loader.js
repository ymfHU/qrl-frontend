import $ from "jquery";
import UserDataStore from "../../stores/UserData";
import NotificationService from "./Notification";

import EventEmitter from "events";

class LoaderService extends EventEmitter {
    constructor() {
        super();
        this.state = {
            showLoader: false
        };
    }

    getState() {
        return this.state.showLoader;
    }

    showLoader() {
        this.state.showLoader = true;
        this.emit("change");
    }

    hideLoader() {
        this.state.showLoader = false;
        this.emit("change");
    }

    isCordova() {
        return document.location.protocol === "file:";
    }

    deleteData(pathname, data, callback, errorCallback) {
        this.loadData(pathname, "DELETE", data, callback, errorCallback);
    }

    putData(pathname, data, callback, errorCallback) {
        this.loadData(pathname, "PUT", data, callback, errorCallback);
    }

    /**
     * Post data
     * @param pathname
     * @param data
     * @param callback
     * @param errorCallback
     */
    postData(pathname, data, callback, errorCallback) {
        this.loadData(pathname, "POST", data, callback, errorCallback);
    }

    getData(pathname, data, callback, errorCallback) {
        pathname =  pathname + "/" + data;
        if (data === "") {
            pathname = pathname.slice(0, -1);
        }
        this.loadData(pathname, "GET", "", callback, errorCallback);
    }

    loadData(pathname, type, data, callback, errorCallback) {
        let self = this,
            token = UserDataStore.getData("token"),
            baseURL = "//api.primelockbox.com/",
            url = baseURL + pathname,
            headers = { "Accept": "application/json"};

        if (token !== "") {
            headers["api-key"] = token;
            console.log("token set to: ", token);
        }

        if (self.isCordova()) {
            url = "https:" + url;
        }

        this.showLoader();
        $.support.cors = true;

        $.ajax({
            url: url,
            data: JSON.stringify(data),
            type: type,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: headers,
            xhrFields: {
                withCredentials: false
            },
            statusCode: {
                404: function() {
                    // No content found (404)
                    // This code will be executed if the server returns a 404 response
                    let result = {status: "Server error", message: "No content found (404)\\n Please try again later!"};

                    NotificationService.showError(result.status, result.message);
                    if (errorCallback) {
                        errorCallback(result);
                        return;
                    }
                    NotificationService.showError(result.status, result.message);
                },
                503: function() {
                    // Service Unavailable (503)
                    // This code will be executed if the server returns a 503 response
                    let result = {status: "Server error", message: "Service Unavailable (503)\n Please try again later!"};

                    if (errorCallback) {
                        errorCallback(result);
                        return;
                    }
                    NotificationService.showError(result.status, result.message);
                },
                500: function() {
                    // Service Unavailable (503)
                    // This code will be executed if the server returns a 503 response
                    let result = {status: "Server error", message: "Internal Server error (500)\n Please try again later!"};

                    if (errorCallback) {
                        errorCallback(result);
                        return;
                    }
                    NotificationService.showError(result.status, result.message);
                }
            },
            success: function (result) {
                self.hideLoader();
                if (result.hasOwnProperty("status") && result.status === 401) {
                    NotificationService.showError(result.name, result.message);
                }

                if (type === "GET" && result.hasOwnProperty("data")) {
                    callback(result.data);
                }
                else {
                    callback(result);
                }
            },
            error: function (error) {
                self.hideLoader();
                if (error.hasOwnProperty("responseJSON")) {
                    NotificationService.showError(error.responseJSON.status, error.responseJSON.message);

                    if (errorCallback) {
                        errorCallback(error.responseJSON);
                    }
                }
            }
        });
    }

    // TODO: remove if everything works fine
    getDataold(pathname, data, callback, errorCallback) {
        this.showLoader();

        let self = this,
            baseURL = "//api.primelockbox.com/",
            url = baseURL + pathname + "/" + data,
            headers = { "Accept": "application/json"};
        // contentType: "application/json; charset=utf-8",

        headers["api-key"] = UserDataStore.getData("token");

        if (data === "") {
            url = url.slice(0, -1);
        }

        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            headers: headers,
            xhrFields: {
                withCredentials: false
            },
            success: function (result) {
                self.hideLoader();
                if (result.hasOwnProperty("status") && result.status === 401) {
                    NotificationService.showError(result.name, result.message);
                }
                callback(result.data);
            },
            error: function (error) {
                self.hideLoader();
                if (error.hasOwnProperty("responseJSON")) {
                    NotificationService.showError(error.responseJSON.status, error.responseJSON.message);

                    if (errorCallback) {
                        errorCallback(error.responseJSON);
                    }
                }
            }
        });
    }
    // TODO: remove if everything works fine - END
}

const loaderService = new LoaderService;

export default loaderService;