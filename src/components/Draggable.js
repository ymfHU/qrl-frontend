/**
 * Import used react components
 */
import React from "react";
import PropTypes from "prop-types";
import { setPrefix } from "react-id-generator";
import nextId from "react-id-generator";

import iScroll from "iscroll";
import ReactIScroll from "react-iscroll";

import onClickOutside from "react-onclickoutside";

class Draggable extends React.Component {
    constructor(props) {
        super( props );

        setPrefix(this.props.useClass + "-");

        this.getRef = "iScroll";
        this.touched = false;

        this.scrollOptions = {
            mouseWheel: true,
            scrollbars: false,
            freeScroll: false,
            invertWheelDirection: true,
            momentum: true
        };

        this.state = {
            uuid: nextId(),
            swiped: false,
            pos: "0px",
            splashColor: this.props.splashColor,
            splashOpacity: 0,
            splashOpacityTransitionTime: 0,
            transition : "0s",
            direction: false,
            containerHeight: 0,
            startTime: 0,
            delta: 0,
            moving: false,
            useClass: this.props.useClass,
            contentClass: this.props.useClass + "-content",
            changeToX: false
        };

        this._swipe = {};
        this.startPos = this.props.startPos;

        this._onTouchStart = this._onTouchStart.bind(this);
        this._onTouchMove = this._onTouchMove.bind(this);
        this._onTouchEnd = this._onTouchEnd.bind(this);
        this._onMouseStart = this._onMouseStart.bind(this);
        this._onMouseMove = this._onMouseMove.bind(this);
        this._onMouseEnd = this._onMouseEnd.bind(this);
        this._setWrapperRef = this._setWrapperRef.bind(this);
        this._handleClickOutside = this._handleClickOutside.bind(this);
    }

    componentDidMount() {
        let self = this,
            max = 0,
            changeToX = false;

        if (this.props.direction === "top" || this.props.direction === "bottom") {
            max = document.getElementById(this.state.uuid).offsetHeight;
        }
        else {
            changeToX = true;
            max = document.getElementById(this.state.uuid).offsetWidth;
        }

        this.setState({ containerHeight: max, changeToX: changeToX });

        const transition = document.querySelector("#" + this.state.uuid + "-container");

        transition.addEventListener("transitionrun", function() {
            self._setOpacity();
        });

        transition.addEventListener("transitionstart", function() {
            self._setOpacity();
        });

        transition.addEventListener("transitioncancel", function() {
            self._setOpacity();
        });

        transition.addEventListener("transitionend", function() {
            self._setOpacity();
        });

        self._setStartPosition(max);
    }

    handleClickOutside() {
        if (this.props.closeOnOutside) {
            this.setState( { pos: "0px", transition: "0.5s" } );
        }
    }

    _handleClickOutside(event) {
        if (((" " + event.target.className + " ").replace(/[\n\t]/g, " ").indexOf(this.state.uuid + "-inner") > -1)){
            return true;
        }

        let self = this;

        console.log(this.state.uuid);
        console.log(this.state.uuid + "-inner");
        console.log(event.target.classList);
        console.log((" " + event.target.className + " ").replace(/[\n\t]/g, " ").indexOf(this.state.uuid + "-inner") > -1);
        console.log(this.state.uuid);

        if (self.touched) { return true; }
        if (self.props.direction === "bottom" || self.props.direction === "right") {
            self.setState({pos: (self.state.containerHeight - self.props.startPos) + "px", transition: "0.5s"});
        }
        else {
            self.setState({pos: (self.props.startPos) + "px", transition: "0.5s"});
        }
        return true;
    }
    _setWrapperRef(node) {
        this.wrapperRef = node;
    }

    _setStartPosition(max) {
        let self = this;

        if (this.props.startPos === null) {
            return;
        }

        setTimeout(function () {
            if (self.props.direction === "bottom" || self.props.direction === "right") {
                self.setState({pos: (max - self.props.startPos) + "px", transition: "0.5s"});
            }
            else {
                self.setState({pos: (self.props.startPos - max) + "px", transition: "0.5s"});
            }
        },500);
    }
    
    _setOpacity() {
        this._swipe.hasOwnProperty("x") ? this.setState({splashOpacityTransitionTime: "0s"}) : this.setState({splashOpacityTransitionTime: "0.2s"});

        if (this.props.direction === "bottom" || this.props.direction === "right") {
            this.setState({splashOpacity: ((this.state.containerHeight - Number(this.state.pos.replace("px",""))) / this.state.containerHeight) - this.props.splashOpacityOffset});
        }
        else {
            this.setState({splashOpacity: 2 - ((this.state.containerHeight - Number(this.state.pos.replace("px",""))) / this.state.containerHeight) - this.props.splashOpacityOffset});
        }
    }

    _onMouseStart(e) {
        this._touchStart(e);
    }

    _onTouchStart(e) {
        const touch = e.touches[0];
        this._touchStart(touch);
    }

    _touchStart(touch) {
        this.touched = true;
        this._swipe = { x: touch.clientX, y: touch.clientY };
        this.startPos = Number(this.state.pos.replace("px",""));
        this.setState({ swiped: false, startTime: Date.now() });
    }

    _calculateDelta(touch) {
        const __swipe = this.state.changeToX ? this._swipe.x : this._swipe.y;
        const __touch = this.state.changeToX ? touch.clientX : touch.clientY;
        const direction = (__touch - __swipe) < 0;
        const delta =  direction ? -Math.abs(__touch - __swipe) : Math.abs(__swipe - __touch );
        const actualPos = Number(this.state.pos.replace("px",""));
        const absY = delta - actualPos;
        const result = this.startPos + actualPos + absY;

        if (direction !== this.state.direction) {
            this._swipe = { x: touch.clientX, y: touch.clientY };
            this.startPos = Number(this.state.pos.replace("px",""));
            this.setState({ swiped: false, startTime: Date.now(), direction: direction, delta: delta });
        }
        else {
            this.setState({direction: direction, delta: delta});
        }

        this._setOpacity();
        return result + "px";
    }

    _onMouseMove(e) {
        if (this.touched) {
            this._swipe.swiping = true;
            this._touchMove(e);
        }
    }

    _onTouchMove(e) {
        if (e.changedTouches && e.changedTouches.length) {
            this._swipe.swiping = true;

            const touch = e.changedTouches[0];
            this._touchMove(touch);
        }
    }

    _touchMove(touch) {
        const delta = this._calculateDelta(touch);

        this.setState({pos: delta, transition: "0s"});
    }

    _onMouseEnd() {
        this._touchEnd();
    }

    _onTouchEnd() {
        this._touchEnd();
    }

    _touchEnd() {
        this.touched = false;
        const timeRate = Date.now()- this.state.startTime;
        let _pos = Number(this.state.pos.replace("px","")) + this.state.delta;
        let _time = 0.5 - ((Math.abs(this.state.delta) / 1000 + timeRate / 100) / 10);
        let minPos = this.state.containerHeight - this.props.minPos;
        let importantPosSet = false;

        if (this.props.direction === "top" || this.props.direction === "left") {
            minPos = this.props.minPos - this.state.containerHeight;
        }

        if (this.props.direction === "top" || this.props.direction === "left") {
            if (_pos <  minPos && this.state.direction) {
                _pos = minPos;
                importantPosSet = true;
            }
            if (_pos > 0 && !this.state.direction) {
                _pos = 0;
                importantPosSet = true;
            }
        }
        else {
            if (_pos < 0 && this.state.direction) {
                _pos = 0;
                console.log(this.state.direction);
                importantPosSet = true;
            }
            if ( _pos > minPos && !this.state.direction) {
                _pos = minPos;
                importantPosSet = true;
            }
        }

        this._swipe = {};

        if (_time > 0) {
            this.setState({ swiped: true, transition: _time + "s", pos: _pos + "px"});
        }
        else if(importantPosSet){
            this.setState({ swiped: true, transition: "0.5s", pos: _pos + "px"});
        }

    }

    _hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? parseInt(result[1], 16) + ", " + parseInt(result[2], 16) + "," + parseInt(result[3], 16) : null;
    }

    render() {
        let self = this;

        const draggableHolderStyle = {
            pointerEvents: "none",
            position: "absolute",
            top: this.props.top,
            left: this.props.left,
            right: this.props.right,
            bottom: this.props.bottom,
            display: this.props.display,
            overflow: "hidden",
            background: "rgba(" + self._hexToRgb(self.props.splashColor) + "," + this.state.splashOpacity + ")",
            transition: "background " + self.state.splashOpacityTransitionTime + " ease-out"
        };

        let draggableContentStyle = {
            pointerEvents: "initial",
            height: "100%",
            transitionTimingFunction: "cubic-bezier(0.1, 0.57, 0.1, 1)",
            transitionDuration: this.state.transition,
            transform: "translateY(" + this.state.pos + ") translateZ(0px)",
            overflow: "hidden",
            touchAction: "none"
        };

        if (this.props.direction === "left" || this.props.direction === "right") {
            draggableContentStyle.transform = "translateX(" + this.state.pos + ") translateZ(0px)";
        }

        let draggableDraggableHolderStyle = {
            pointerEvents: "all",
            width: "100%",
            height: "30px",
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: "unset",
            cursor: "pointer"
        };

        if (this.props.direction === "top") {
            draggableDraggableHolderStyle.bottom = 0;
            draggableDraggableHolderStyle.top = "unset";
        }

        if (this.props.direction === "left") {
            draggableDraggableHolderStyle.bottom = 0;
            draggableDraggableHolderStyle.top = 0;
            draggableDraggableHolderStyle.left = "unset";
            draggableDraggableHolderStyle.right = 0;
            draggableDraggableHolderStyle.width = "30px";
            draggableDraggableHolderStyle.height = "100%";
        }

        let draggableDraggablDraggableeStyle = {
            position: "absolute",
            top: 0,
            left: "50%",
            width: "20px",
            height: "2px",
            backgroundColor: this.props.dragIconColor,
            content: " ",
            transform: "translate(-10px, 15px)"
        };

        if (this.props.direction === "left") {
            draggableDraggablDraggableeStyle.left = "unset";
            draggableDraggablDraggableeStyle.width = "2px";
            draggableDraggablDraggableeStyle.height = "20px";
            draggableDraggablDraggableeStyle.top = "50%";
            // draggableDraggablDraggableeStyle.right = 0;
            draggableDraggablDraggableeStyle.transform =  "translate(15px, -10px)";
        }

        let draggableMenuStyle = {
            pointerEvents: "all",
            position: "absolute",
            top: "200px",
            left: "50%",
        };

        if (this.props.display === "contents") {
            draggableMenuStyle.top = 0;
            draggableMenuStyle.left = 0;
            draggableMenuStyle.right = 0;
        }

        var children = React.Children.map(
            this.props.children,
            function(child) {
                return <div
                    style={draggableContentStyle}
                    id={self.state.uuid + "-container"}
                    className={self.state.contentClass}
                >
                    <div
                        style={draggableDraggableHolderStyle}
                        className={self.state.useClass + "-draggable-holder " + self.state.uuid + "-inner"}
                        onTouchStart={self._onTouchStart}
                        onTouchMove={self._onTouchMove}
                        onTouchEnd={self._onTouchEnd}
                        onMouseDown={self._onMouseStart}
                        onMouseMove={self._onMouseMove}
                        onMouseUp={self._onMouseEnd}
                        onMouseOut={self._onMouseEnd}
                    >
                        <div
                            style={draggableDraggablDraggableeStyle}
                            className={self.state.useClass + "-draggable-draggable"}
                        />
                    </div>

                    { self.props.iscroll ?
                        <ReactIScroll
                            ref={self.getRef}
                            iScroll={iScroll}
                            options={self.scrollOptions}
                        >{child}</ReactIScroll> : <div>{child}</div> }
                </div>;
            }
        );
        return <div
            style={draggableHolderStyle}
            id={this.state.uuid}
            className={this.state.useClass}
        >
            <div
                style={draggableMenuStyle}
                className={self.state.useClass + "-menu"}
            >
            </div>
            {children}
        </div>;
    }
}

// Accepts either a single field, or an array of fields
// Note: Reject a Form with no children
Draggable.propTypes = {
    children: PropTypes.shape({
        type: PropTypes.any
    }),
    startPos: PropTypes.number,
    minPos: PropTypes.number,
    splashColor: PropTypes.string,
    dragIconColor: PropTypes.string,
    useClass: PropTypes.string,
    splashOpacity: PropTypes.number,
    splashOpacityOffset: PropTypes.number,
    top: PropTypes.string,
    left: PropTypes.string,
    right: PropTypes.string,
    bottom: PropTypes.string,
    direction: PropTypes.string,
    iscroll: PropTypes.bool,
    display: PropTypes.string,
    closeOnOutside: PropTypes.bool,
};

Draggable.defaultProps = {
    startPos: null,
    minPos: "30",
    splashColor: "#000000",
    dragIconColor: "#ffffff",
    useClass: "ymf-draggable",
    splashOpacity: 1,
    splashOpacityOffset: 0.2,
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    direction: "bottom",
    iscroll: true,
    display: "block",
    closeOnOutside: false
};

export default onClickOutside(Draggable);