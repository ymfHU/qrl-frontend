import React from "react";
import IndicatorStore from "../stores/Indicator";

export default  class Indicator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            indicator: IndicatorStore.getValue("indicator")
        };

        IndicatorStore.on("change", () => {
            this.setState({
                indicator: IndicatorStore.getValue("indicator"),
            });
        });
    }

    render()
    {
        return (
            <div className={this.state.indicator.length === 0 ? "indicator-holder" : "indicator-holder active"}>
                <ul className="indicator">
                    <li className="line">&nbsp;</li>
                    {this.state.indicator.map((item, i) => {
                        return (
                            <React.Fragment  key={i}>
                                <li data-type="options" className="circle-contain">
                                    <span className="label">{item.label}</span>
                                    <div className={"circle " + item.status}>
                                        <span className="count">{item.count}</span>
                                    </div>
                                </li>
                                <li className={i+1 < this.state.indicator.length ? "line middle" : "hidden"}>&nbsp;</li>
                            </React.Fragment>
                        );
                    })}
                    <li className="line line">&nbsp;</li>
                </ul>
            </div>
        );
    }
}