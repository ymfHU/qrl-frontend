import React, { Component } from "react";
import {Route, Switch} from "react-router-dom";
import {
    CSSTransition,
    TransitionGroup,
} from "react-transition-group";

import Login from "./Login";
import RegStep1 from "./registration/Step1";
import RegStep2 from "./registration/Step2";
import RegStep3 from "./registration/Step3";
import RegStep4 from "./registration/Step4";
import RegStep5 from "./registration/Step5";
import RegStep6 from "./registration/Step6";
import RegFinish from "./registration/Finish";
import MyLocations from "./MyLocations";
import Details from "./Details";

import About from "./About";
// import Error from "./Error";
import Menu from "./header/Menu_btn";
import Header from "./header/Header";
import PageHandler from "./services/PageHandler";
import Loader from "./Loader";
import Notification from "./Notification";
import IndicatorStore from "../stores/Indicator";
import PageHandlerStore from "../stores/PageHandler";
// import Scroll from "./Scroll";


// import IScroll from "iscroll-react";


// import iScroll from "iscroll";
// import ReactIScroll from "react-iscroll";


// import iScroll from "iscroll";
//
// import ReactIScroll from "react-iscroll";

import IScroll from "iscroll-react";
import iscroll from "iscroll";

export default class App extends Component {

    getDefaultProps() {
        return ({
            mouseWheel: true,
            scrollbars: true
        });
    }

    onScrollStart() {
        console.log("iScroll starts scrolling");
    }

    constructor(props) {
        super(props);

        this.state = {
            indicator: IndicatorStore.getValue("indicator"),
            subtitle: PageHandlerStore.getValue("subtitle"),
        };
    }

    componentDidMount() {
        IndicatorStore.on("change", () => {
            this.setState({
                indicator: IndicatorStore.getValue("indicator"),
            });
        });

        PageHandlerStore.on("change", () => {
            this.setState({
                subtitle: PageHandlerStore.getValue("subtitle"),
            });
        });
    }

    getMainClass() {
        var _classes = "";

        if (this.state.indicator.length > 0) {
            _classes +="indicator-on";
        }

        if (this.state.subtitle.length > 0) {
            _classes +=" subtitle-on";
        }

        return _classes;
    }

    render() {
        return (
            <div>
                <Notification />
                <Loader />
                <PageHandler />
                <Menu />
                <Header />
                <main className={this.getMainClass()}>
                    <IScroll iScroll={iscroll}>
                        <Route render={({location}) => (
                            <TransitionGroup>
                                <CSSTransition
                                    key={location.key}
                                    timeout={450}
                                    classNames="fade"
                                >
                                    <Switch location={location}>
                                        <Route path="/" component={Login} exact />
                                        <Route path="/about" component={About} />
                                        <Route path="/registration/step1" component={RegStep1} />
                                        <Route path="/registration/step2" component={RegStep2} />
                                        <Route path="/registration/step3" component={RegStep3} />
                                        <Route path="/registration/step4" component={RegStep4} />
                                        <Route path="/registration/step5" component={RegStep5} />
                                        <Route path="/registration/step6" component={RegStep6} />
                                        <Route path="/registration/finish" component={RegFinish} />
                                        <Route path="/my-locations" component={MyLocations} />
                                        <Route path="/details" component={Details} />
                                        <Route path="/edit" component={RegStep5} />
                                        <Route path="/review" component={RegStep6} />
                                        <Route path="/scan" component={RegStep3} />
                                        <Route path="/add-location/step1" component={RegStep3} />
                                        <Route path="/add-location/step2" component={RegStep4} />
                                        <Route path="/add-location/step3" component={RegStep5} />
                                        <Route path="/add-location/step4" component={RegStep6} />
                                        <Route path="/add-location/finish" component={RegFinish} />
                                        <Route component={Login} />
                                    </Switch>
                                </CSSTransition>
                            </TransitionGroup>
                        )} />
                    </IScroll>
                </main>
            </div>
        );
    }
}