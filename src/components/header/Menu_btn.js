import React from "react";
// import menu_icon from "../../images/menu.svg";
import Menu from "./Menu";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";

import PageHandlerStore from "../../stores/PageHandler";
import userDataStore from "../../stores/UserData";

export default class Menu_btn extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            token: userDataStore.getData("token"),
            menu_active: false
        };

        userDataStore.on("change", () => {
            this.setState({
                token: userDataStore.getData("token"),
            });
        });

        PageHandlerStore.on("change", () => {
            this.setState({
                menu_active: PageHandlerStore.getValue("menuActive"),
            });
        });

        this.clickMenu = this.clickMenu.bind(this);
    }

    clickMenu(e) {
        e.preventDefault();
        PageHandlerStore.setVariable("menuActive", !PageHandlerStore.getValue("menuActive"));
    }

    render()
    {
        return (
            <div>
                <menu className={"" + this.state.menu_active}>
                    { this.state.token !== "" ?<div className="menu_btn" onClick={this.clickMenu}><FontAwesomeIcon icon={faBars}/></div> : null }
                    <div className={"menu_bg " + this.state.menu_active}>
                        <Menu />
                    </div>
                </menu>
                <div className="footer_bg" />
                <div className="footer_bg2" />
            </div>
        );
    }
}