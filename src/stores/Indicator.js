import EventEmitter from "events";

class IndicatorStore extends EventEmitter {
    constructor() {
        super();
        this.state = {
            indicator: [],
        };
    }

    getAll() {
        return this.state;
    }

    getValue(variable) {
        return this.state[variable];
    }

    setVariable(variable, value) {
        this.state[variable] = value;
        // console.log("goToPage changed to: ", value);
        this.emit("change");
    }
}

const indicatorStore = new IndicatorStore;

export default indicatorStore;