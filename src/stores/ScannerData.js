import EventEmitter from "events";

class ScannerDataStore extends EventEmitter {
    constructor(listenerKey) {
        super();

        this.listenerKey = listenerKey;

        this.state = {
            qrCode: "",
            address1: "",
            address2: "",
            boxtype_name: "",
            city: "",
            colorcode_name: "",
            description: "",
            id: "",
            keypadtype_name: "",
            keypadtype_version: "",
            zip: ""
        };
    }

    emitChange() {
        setTimeout(() => {
            this.emit(this.listenerKey);
        }, 0);
    }

    addChangeListener(callback) {
        this.on(this.listenerKey, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(this.listenerKey, callback);
    }

    getData(variable) {
        return this.state[variable];
    }

    setData(variable, data) {
        console.log("setdata ", variable, data);
        this.state[variable] = data;
        this.emit("change");
    }

    subsribe(listenerKey, callback) {
        this.on(listenerKey, callback);
    }

    unsubscribe(listenerKey, callback) {
        this.removeListener(listenerKey, callback);
    }
}

const scannerDataStore = new ScannerDataStore;

export default scannerDataStore;