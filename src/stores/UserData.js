import EventEmitter from "events";

class UserDataStore extends EventEmitter {
    constructor() {
        super();
        this.userdata = {
            id: "",
            role: "",
            username: "",
            password: "",
            token: "",
            qrCode: ""
        };
    }

    getData(variable) {
        return this.userdata[variable];
    }

    setData(variable, data) {
        this.userdata[variable] = data;
        this.emit("change");
    }
}

const userDataStore = new UserDataStore;

export default userDataStore;