import EventEmitter from "events";

class QRDataStore extends EventEmitter {
    constructor() {
        super();
        this.state = {
            qrcode: "",
            address1: "",
            address2: "",
            boxtype_name: "",
            city: "",
            colorcode_name: "",
            description: "",
            id: "",
            keypadtype_name: "",
            keypadtype_version: "",
            zip: "",
            lockcode: "",
            lock_description: ""
        };
    }

    getDataAll() {
        return this.state;
    }

    resetData() {
        this.state = {
            qrcode: "",
            address1: "",
            address2: "",
            boxtype_name: "",
            city: "",
            colorcode_name: "",
            description: "",
            id: "",
            keypadtype_name: "",
            keypadtype_version: "",
            zip: "",
            lockcode: "",
            lock_description: "",
            error: false,
            facingMode: "rear"
        };
    }

    getData(variable) {
        return this.state[variable];
    }

    setData(variable, data) {
        if (typeof(variable) === "object") {
            for (let key in variable) {
                this.state[key] = variable[key];
            }
        }
        else {
            this.state[variable] = data;
        }
        this.emit("change");
    }
}

const qRDataStore = new QRDataStore;

export default qRDataStore;