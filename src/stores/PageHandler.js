import EventEmitter from "events";

class PageHandlerStore extends EventEmitter {
    constructor() {
        super();
        this.state = {
            isDarkMode: true,
            goToPage: "",
            menuActive: false,
            title: "",
            subtitle: ""
        };
    }

    getAll() {
        return this.state;
    }

    getValue(variable) {
        return this.state[variable];
    }

    setVariable(variable, value) {
        if (this.state[variable] !== value) {
            this.state[variable] = value;
            this.emit("change");
        }
    }
}

const pageHandlerStore = new PageHandlerStore;

export default pageHandlerStore;