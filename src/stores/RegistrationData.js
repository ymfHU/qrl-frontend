import EventEmitter from "events";

class RegistrationDataStore extends EventEmitter {
    constructor() {
        super();
        this.registration = {
            id: "",
            role: "",
            firstname: "",
            middlename: "",
            lastname: "",
            email: "",
            email_verification_code: "",
            password: "",
            password_confirm: "",
            address: "",
            country: "USA",
            state: "",
            city: "",
            zip: "",
            lockcode: ""
        };
    }

    resetData() {
        this.registration = {
            id: "",
            role: "",
            firstname: "",
            middlename: "",
            lastname: "",
            email: "",
            email_verification_code: "",
            password: "",
            password_confirm: "",
            address: "",
            country: "USA",
            state: "",
            city: "",
            zip: "",
            lockcode: ""
        };
    }

    getData(variable) {
        return this.registration[variable];
    }

    setData(variable, data) {
        this.registration[variable] = data;
        this.emit("change");
    }

    getDataAll() {
        return this.registration;
    }
}

const registrationDataStore = new RegistrationDataStore;

export default registrationDataStore;