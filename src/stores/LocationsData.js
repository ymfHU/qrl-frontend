import EventEmitter from "events";

class LocationsDataStore extends EventEmitter {
    constructor() {
        super();
        this.state = {};
    }

    getDataAll() {
        return this.state;
    }

    resetData() {
        this.state = {
            data: []
        };
    }

    getData(variable) {
        return this.state[variable];
    }

    setData(variable, data) {
        if (typeof(variable) === "object") {
            for (let key in variable) {
                console.log(key);
                this.state[key] = variable[key];
            }
        }
        else {
            this.state[variable] = data;
        }
        this.emit("change");
    }
}

const locationsDataStore = new LocationsDataStore;

export default locationsDataStore;