// // PACKAGE DEPENDENCIES
// import React from "react";
// import ReactDOM from "react-dom";
// import {BrowserRouter, Route} from "react-router-dom";
//
// // COMPONENTS
//
// import "normalize.css";
// import "./styles/app.scss";
// import App from "./components/App";
//
// const startApp = () => {
//     // navigator.splashscreen.hide();
//     ReactDOM.render(
//         <BrowserRouter>
//             <Route path="/" component={App} />
//         </BrowserRouter>
//         , document.getElementById("app")
//     );
// };


import React from "react";
import ReactDOM from "react-dom";
// import { Provider } from "react-redux";
// import { createStore, applyMiddleware } from "redux";
// import { Router, Route, browserHistory} from "react-router";

import "normalize.css";
import "./styles/app.scss";


// import routes from "./routes";
// import Notification from "./components/Notification";
// import Loader from "./components/Loader";
// import PageHandler from "./components/services/PageHandler";
// import Menu from "./components/header/Menu_btn";
// import Header from "./components/header/Header";
// import Login from "./components/Login";
// import About from "./components/About";
// import RegStep1 from "./components/registration/Step1";
// import RegStep2 from "./components/registration/Step2";
// import RegStep3 from "./components/registration/Step3";
// import RegStep4 from "./components/registration/Step4";
// import RegStep5 from "./components/registration/Step5";
// import RegStep6 from "./components/registration/Step6";
// import RegFinish from "./components/registration/Finish";
// import MyLocations from "./components/MyLocations";
// import Details from "./components/Details";
// import Notification from "./components/Notification";
// import Loader from "./components/Loader";
// import PageHandler from "./components/services/PageHandler";
// import Menu from "./components/header/Menu_btn";

import { eventListenerOptionsSupported } from "./utils";
const defaultOptions = {
    passive: true,
    capture: false
};
const supportedPassiveTypes = [
    "scroll", "wheel",
    "touchstart", "touchmove", "touchenter", "touchend", "touchleave",
    "mouseout", "mouseleave", "mouseup", "mousedown", "mousemove", "mouseenter", "mousewheel", "mouseover"
];
const getDefaultPassiveOption = (passive, eventName) => {
    if (passive !== undefined) return passive;

    return supportedPassiveTypes.indexOf(eventName) === -1 ? false : defaultOptions.passive;
};

const getWritableOptions = (options) => {
    const passiveDescriptor = Object.getOwnPropertyDescriptor(options, "passive");

    return passiveDescriptor && passiveDescriptor.writable !== true && passiveDescriptor.set === undefined
        ? Object.assign({}, options)
        : options;
};

const overwriteAddEvent = (superMethod) => {
    EventTarget.prototype.addEventListener = function (type, listener, options) {
        const usesListenerOptions = typeof options === "object" && options !== null;
        const useCapture          = usesListenerOptions ? options.capture : options;

        options         = usesListenerOptions ? getWritableOptions(options) : {};
        options.passive = getDefaultPassiveOption(options.passive, type);
        options.capture = useCapture === undefined ? defaultOptions.capture : useCapture;

        superMethod.call(this, type, listener, options);
    };

    EventTarget.prototype.addEventListener._original = superMethod;
};

const supportsPassive = eventListenerOptionsSupported();

if (supportsPassive) {
    const addEvent = EventTarget.prototype.addEventListener;
    overwriteAddEvent(addEvent);
}

import App from "./components/App";

const startApp = () => {
    ReactDOM.render(
        <App />
        , document.getElementById("app"));
};



document.addEventListener("DOMContentLoaded", function(){
    if(!window.cordova) {
        console.log("start");
        startApp();
    } else {
        document.addEventListener("deviceready", startApp, false);
        // document.addEventListener("online", startApp, false);
        // document.addEventListener("offline", onDeviceOffline, false); //TODO: Handle offline state if needed
    }
}, false);
